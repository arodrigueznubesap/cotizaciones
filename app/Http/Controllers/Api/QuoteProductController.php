<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\InstallationParameter;
use App\Models\InstallationTemplate;
use App\Models\Product;
use App\Models\Quote;
use App\Models\QuoteProduct;
use App\Models\QuoteInstallation;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class QuoteProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $addProd = QuoteProduct::create($request->all());


            /* $addProd->id_quote = $request->id_quote;
            //$addProd->id_product = $request->id_product;
            $addProd->level = $request->level;
            //$addProd->title = $request->title;
            //$addProd->is_title = $request->is_title;

            $addProd->unit = $request->unit;
            $addProd->ggenerales_inst = 10.00;
            //$addProd->quantity = $request->quantity;
            //$addProd->unit_price = $request->unit_price;
            //$addProd->costo_unit_dollar = $request->costo_unit_dollar;
            //$addProd->kg_x_unit = $request->kg_x_unit;
            //$addProd->dollar_x_unit = $request->dollar_x_unit; */



            //$addProd->save($request->all());
            $alldata  = QuoteProduct::find($addProd->id);
            if ($alldata) {
                return ([
                    "status" => 1,
                    "data" => $alldata,
                    "error" => "",
                    "message" => ""
                ]);
            } else {
                return ([
                    "status" => 0,
                    "data" => [],
                    "error" => "Error",
                    "message" => "Ocurrio un Error al Crear la fila"
                ]);
            }
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => $e,
                "message" => "Ocurrio un Error al Crear la fila"
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QuoteProduct  $quoteProduct
     * @return \Illuminate\Http\Response
     */
    public function show(QuoteProduct $quoteProduct)
    {

        try {
            $data = QuoteProduct::where('quote_products.id', '=', $quoteProduct->id)
                ->join('products', 'quote_products.id_product', '=', 'products.id')
                ->select('products.id as id_product', 'quote_products.id as id_quote_product', 'products.*', 'quote_products.*')
                ->first();



            $qp = QuoteInstallation::where('id_quote_products', '=', $quoteProduct->id)
                ->join('installation_parameters as t', 'quote_installations.id_installation_parameter', '=', 't.id')
                ->select('t.id as id_installation_parameter', 'quote_installations.id as id_qip', 'quote_installations.*', 't.*')
                ->get();

            //echo $qp;

            return ([
                "status" => 1,
                "data" => [
                    'quote_product' => $data,
                    'quote_installation' => $qp
                ],
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => $e,
                "message" => "No se encontró ese detalle del producto"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QuoteProduct  $quoteProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuoteProduct $quoteProduct)
    {
        try {
            //$quoteProduct->id_quote = $request->id_quote;
            /*             $quoteProduct->id_product = $request->id_product;
            $quoteProduct->level = $request->level;
            $quoteProduct->title = $request->title;
            $quoteProduct->is_title = $request->is_title;
            $quoteProduct->unit = $request->unit;
            $quoteProduct->quantity = $request->quantity;
            $quoteProduct->unit_price = $request->unit_price;
            $quoteProduct->costo_x_unit_dollar = $request->costo_unit_dollar;
            $quoteProduct->kg_x_unit = $request->kg_x_unit;
            $quoteProduct->dollar_x_unit = $request->dollar_x_unit;
            $quoteProduct->subtotal = $request->subtotal;
            $quoteProduct->save(); */

            $quoteProduct->update($request->all());

            /*if ($quoteProduct->quantity > 0) {
                $quoteProduct->usd_x_m2_inst =  $quoteProduct->total_inst / $quoteProduct->quantity;
            }
            else{
                $quoteProduct->usd_x_m2_inst = 0;
            }*/
            $quoteProduct->save();
            //print_r($request->all());
            $prod = Product::find($quoteProduct->id_product);



            /* $installParam = QuoteInstallation::where('id_quote_products', '=', $quoteProduct->id)->get();
            $installTemplate = InstallationTemplate::where('id_installation_type', '=', $prod->id_installation_type)
                ->join('installation_parameters as p', 'installation_templates.id_installation_parameter', '=', 'p.id')
                ->select('p.id as id_installation_parameter', 'installation_templates.*', 'p.*')
                ->get();*/

            $qp = QuoteInstallation::where('id_quote_products', '=', $quoteProduct->id)
                ->join('installation_parameters as t', 'quote_installations.id_installation_parameter', '=', 't.id')
                ->select('t.id as id_installation_parameter', 'quote_installations.*', 't.*')
                ->get();



            if (count($qp) > 0) {
                //Aquí se debería eliminar los parametros o cambiarlos por el template correcto
                //Log::info("Aquí se debería ir la desición de eliminar o no, los parametros o cambiarlos por el template correcto en el caso que se cambie el tipo de producto");
                //echo "MAL 1";
            } else {

                //echo "MAL 2";
                if (!is_null($qp)) {
                    //echo "MAL 3";
                    //$installParam = QuoteInstallation::where('id_quote_products', '=', $quoteProduct->id)->get();
                    $installTemplate = InstallationTemplate::where('id_installation_type', '=', $prod->id_installation_type)
                        ->join('installation_parameters as p', 'installation_templates.id_installation_parameter', '=', 'p.id')
                        ->select('p.id as id_installation_parameter', 'installation_templates.*', 'p.*')
                        ->get();

                    //echo "Typ2:  ".$installTemplate;
                    foreach ($installTemplate as $it) {
                        //echo "MAL 3";
                        $tmp = new QuoteInstallation();
                        $tmp->id_quote_products = $quoteProduct->id;
                        $tmp->id_installation_parameter = $it->id_installation_parameter;
                        $tmp->level_class = $it->level;
                        $tmp->report_description = $it->description;
                        $tmp->save();
                    }
                }
            }

            $qp = QuoteInstallation::where('id_quote_products', '=', $quoteProduct->id)
                ->join('installation_parameters as t', 'quote_installations.id_installation_parameter', '=', 't.id')
                ->select('t.id as id_installation_parameter', 'quote_installations.id as id_qip', 'quote_installations.*', 't.*')
                ->get();

            //echo $qp;

            if ($quoteProduct) {
                return ([
                    "status" => 1,
                    "data" => [
                        "quote_product" => $quoteProduct,
                        "installation_data" => $qp
                    ],
                    "error" => "",
                    "message" => ""
                ]);
            } else {
                return ([
                    "status" => 0,
                    "data" => [],
                    "error" => "Error",
                    "message" => "Ocurrio un Error al Crear la fila"
                ]);
            }
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => $e,
                "message" => "$e"
            ]);
        }
    }

    public function installationParams($id_quote)
    {
        try {

            $q = Quote::where('id', '=', $id_quote)->first();
            $qt = new QuoteController();
            $quo = $qt->show($q);


            //print_r($quo->object());

            $inst = $quo['dataAll']['quote_products'];
            $suma = null;
            $item = null;



            foreach ($inst as $i) {
                $qi = $i->quote_installations;
                foreach ($qi as $j) {

                    if (isset($suma[$j->id_installation_parameter]) && $suma[$j->id_installation_parameter] > 0) {
                        $suma[$j->id_installation_parameter] = $suma[$j->id_installation_parameter] + $j->quantity;
                        //echo "(((INGRESO1: ".$j->id_quote_installation. "QUANT1: ".$j->quantity.")))-----------";

                    } else {
                        if ($j->quantity > 0) {
                            $suma[$j->id_installation_parameter] = $j->quantity;
                            //echo "(((INGRESO2: ".$j->id_quote_installation. "QUANT2: ".$j->quantity.")))------------";

                        }
                    }

                    if (isset($suma[$j->id_installation_parameter])) {
                        $item[$j->id_installation_parameter] = ['level_class' => $j->level_class, 'report_description' => $j->description, 'suma' => $suma[$j->id_installation_parameter]];
                    }
                }
            }


            /* $qp = QuoteInstallation::where('id_quote_products', '=', $id_quote_product)
                ->join('installation_parameters as t', 'quote_installations.id_installation_parameter', '=', 't.id')
                ->select('t.id as id_installation_parameter', 'quote_installations.*', 't.*')
                ->get(); */
            return ([
                "status" => 1,
                "data" => $item,
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => $e,
                "message" => "$e"
            ]);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuoteProduct  $quoteProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuoteProduct $quoteProduct)
    {
        try {

            DB::beginTransaction();
            if ($quoteProduct->is_title == 1) {
                $delete = QuoteProduct::where('id_quote', '=', $quoteProduct->id_quote)
                ->where('level', '>=', $quoteProduct->level)
                ->where('level', '<', $quoteProduct->level+1);
                
                foreach ($delete->get() as $del) {
                    $qip = QuoteInstallation::where('id_quote_products', '=', $del->id);
                    $qip->delete();
                    //$quoteProduct->delete();
                    //echo "Titulo: " . $qip->get()."<br>";    
                }
                $delete2 = $delete->get();
                $delete->delete();
                //echo "AQUI: " . $delete2;
                $upd = QuoteProduct::where('id_quote', '=', $quoteProduct->id_quote)
                ->where('level', '>', $quoteProduct->level)
                ->decrement('level', 1);

                //echo "UPDA: " . $upd . "FIN-----------------------------------------------";

                
            } else {
                $qip = QuoteInstallation::where('id_quote_products', '=', $quoteProduct->id);
                //echo "No titulo: " . $qip->get()."<br>";
                $qip->delete();
                $quoteProduct->delete();

                if(intval($quoteProduct->level)!=$quoteProduct->level){
                    $upd = QuoteProduct::where('id_quote', '=', $quoteProduct->id_quote)
                    ->where('level', '>', $quoteProduct->level) //1.2
                    ->where('level', '<', intval($quoteProduct->level) + 1) //2 
                    ->decrement('level', 0.1);
                }
                else{
                    $upd = QuoteProduct::where('id_quote', '=', $quoteProduct->id_quote)
                    ->where('level', '>', $quoteProduct->level)
                    ->decrement('level', 1);
                }
            }



            DB::commit();
            return ([
                "status" => 1,
                "data" => $quoteProduct,
                "error" => "",
                "message" => "Se borró con éxito"
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            return ([
                "status" => 0,
                "data" => [],
                "error" => $e,
                "message" => "Ocurrio un Error al Eliminar"
            ]);
        }
    }
}
