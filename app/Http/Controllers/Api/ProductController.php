<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\InstallationTemplate;
use App\Models\InstallationType;
use App\Models\Product;
use App\Models\ProductBrand;
use App\Models\ProductType;
use Exception;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Product::all();

        $data = $data->map(function ($product) {

            $product->product_type = ProductType::where('id', '=', $product->id_product_type)->first();
            $product->product_brand = ProductBrand::where('id', '=', $product->id_product_brand)->first();
            $product->installation_type = InstallationType::where('id', '=', $product->id_installation_type)->first();
            //$product->installation_template = InstallationTemplate::where('id_installation_type', '=', $product->id_installation_type)->get();
            return $product;
        });

        if (count($data) > 0) {


            return ([
                "status" => 1,
                "data" =>  $data,
                "error" => "",
                "message" => ""
            ]);
        } else {
            return ([
                "status" => 0,
                "data" => [],
                "error" => "Error 1",
                "message" => "No se encontraron datos"
            ]);
        }
    }

    public function list($page)
    {
        $data2 = Product::all();
        $total = count($data2) - 1;

        $data = Product::where('id', '>', '1')->limit(15)->offset(($page-1)*15)->get();
        //echo $data;
        $data = $data->map(function ($product) {

            $product->product_type = ProductType::where('id', '=', $product->id_product_type)->first();
            $product->product_brand = ProductBrand::where('id', '=', $product->id_product_brand)->first();
            $product->installation_type = InstallationType::where('id', '=', $product->id_installation_type)->first();
            //$product->installation_template = InstallationTemplate::where('id_installation_type', '=', $product->id_installation_type)->get();
            return $product;
        });
        $total2 = count($data);
        if ($total2 > 0) {


            return ([
                "status" => 1,
                "data" =>  $data,
                "total" => $total,
                "error" => "",
                "message" => ""
            ]);
        } else {
            return ([
                "status" => 0,
                "data" => [],
                "error" => "Error 1",
                "message" => "No se encontraron datos"
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $prod = new Product();
            $prod->code = $request->code;
            $prod->description = $request->description;
            $prod->unit = $request->unit;
            $prod->id_product_brand = 1;
            $prod->id_product_type = $request->id_product_type;
            $prod->id_installation_type = $request->id_installation_type;
            $prod->unit_price = $request->unit_price;
            $prod->save();
            return ([
                "status" => 1,
                "data" =>  $prod,
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" =>  [],
                "error" => $e,
                "message" => "Error Interno"
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
       

        if ($product) {
            return ([
                "status" => 1,
                "data" => $product,
                "error" => "",
                "message" => ""
            ]);
        } else {
            return ([
                "status" => 0,
                "data" => [],
                "error" => "Error 1",
                "message" => "No hubieron Coincidencias"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        try {
            
            /* $product->code = $request->code;
            $product->description = $request->description;
            $product->unit = $request->unit;
            $product->id_product_brand = 1;
            $product->id_product_type = $request->id_product_type;
            $product->id_installation_type = $request->id_installation_type;
            $product->unity_price = $request->unity_price;
            $product->save(); */

            $product->update($request->all());
            return ([
                "status" => 1,
                "data" =>  $product,
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" =>  [],
                "error" => $e,
                "message" => "Error Interno"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
