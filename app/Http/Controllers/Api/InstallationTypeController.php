<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\InstallationType;
use Illuminate\Http\Request;

class InstallationTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InstallationType  $installationType
     * @return \Illuminate\Http\Response
     */
    public function show(InstallationType $installationType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InstallationType  $installationType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InstallationType $installationType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InstallationType  $installationType
     * @return \Illuminate\Http\Response
     */
    public function destroy(InstallationType $installationType)
    {
        //
    }
}
