<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\condition;
use Illuminate\Http\Request;

class ConditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\condition  $condition
     * @return \Illuminate\Http\Response
     */
    public function show(condition $condition)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\condition  $condition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, condition $condition)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\condition  $condition
     * @return \Illuminate\Http\Response
     */
    public function destroy(condition $condition)
    {
        //
    }
}
