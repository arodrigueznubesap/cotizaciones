<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Exception;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function search($term)
    {

        //return json_encode($term);
        $query = Customer::where('name', 'like', '%' . $term . '%')->get();

        if ($query) {
            return ([
                "status" => 1,
                "data" => $query,
                "error" => "",
                "message" => ""
            ]);
        } else {
            return ([
                "status" => 0,
                "data" => [],
                "error" => "Error 1",
                "message" => "Ocurrió un Error"
            ]);
        }
    }

    public function index()
    {

        $data = Customer::all();

        //return $data;

        if ($data) {
            return ([
                "status" => 1,
                "data" => $data,
                "error" => "",
                "message" => ""
            ]);
        } else {
            return ([
                "status" => 0,
                "data" => [],
                "error" => "Error 1",
                "message" => "Ocurrió un Error"
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer();
        $customer->name = $request->name;
        $customer->document_type = $request->document_type;
        $customer->address = $request->address;
        $customer->ruc = $request->ruc;
        $customer->phone = $request->phone;
        $customer->email = $request->email;

        $customer->save();

        if ($customer) {
            return ([
                "status" => 1,
                "data" => $customer,
                "message" => ""
            ]);
        } else {
            return ([
                "status" => 0,
                "error" => "Error"
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(customer $customer)
    {


        try {
            $data = Customer::find($customer);

            //return $data;

            if ($data) {
                return ([
                    "status" => 1,
                    "data" => $data,
                    "error" => "",
                    "message" => ""
                ]);
            } else {
                return ([
                    "status" => 0,
                    "data" => [],
                    "error" => "Error 1",
                    "message" => "Ocurrió un Error"
                ]);
            }
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => "Error 1",
                "message" => "Ocurrió un Error"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        /* $customer->name = $request->name;
        $customer->ruc = $request->ruc;
        $customer->phone = $request->phone;
        $customer->email = $request->email;


        $customer->save(); */
        try {
            $customer->update($request->all());

            if ($customer) {
                return ([
                    "status" => 1,
                    "data" => $customer,
                    "message" => ""
                ]);
            } else {
                return ([
                    "status" => 0,
                    "error" => "Error"
                ]);
            }
        } catch (Exception $e) {
            return ([
                "status" => 1,
                "data" => [],
                "error" => "$e",
                "message" => ""
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }
}
