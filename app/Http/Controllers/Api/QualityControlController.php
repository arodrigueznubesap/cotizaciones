<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\QualityAttribute;
use App\Models\QualityControl;
use Exception;
use Illuminate\Http\Request;

class QualityControlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QualityControl  $qualityControl
     * @return \Illuminate\Http\Response
     */
    public function show(QualityControl $qualityControl)
    {

        try {
            $data = QualityControl::where('quality_controls.id', '=', $qualityControl->id)
                ->join('quality_attributes', 'quality_controls.id_quality_attribute', '=', 'quality_attributes.id')
                ->get();
            //$attr = QualityAttribute::where('id', '=', $qualityControl->id_quality_attribute);
            return ([
                "status" => 1,
                "data" => $data,
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => $e,
                "message" => "No se encontraron datos"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QualityControl  $qualityControl
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QualityControl $qualityControl)
    {
        try {
            $qualityControl->state = $request->state;
            $qualityControl->save();
            return ([
                "status" => 1,
                "data" => $qualityControl,
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 1,
                "data" => [],
                "error" => $e,
                "message" => ""
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QualityControl  $qualityControl
     * @return \Illuminate\Http\Response
     */
    public function destroy(QualityControl $qualityControl)
    {
        //
    }


}
