<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Condition;
use App\Models\Customer;
use App\Models\Product;
use App\Models\QualityAttribute;
use App\Models\QualityControl;
use App\Models\Quote;
use App\Models\QuoteCondition;
use App\Models\QuoteInstallation;
use App\Models\QuoteProduct;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Quote::all();

        //return $data;

        if ($data) {
            return ([
                "status" => 1,
                "data" => $data,
                "error" => "",
                "message" => ""
            ]);
        } else {
            return ([
                "status" => 0,
                "data" => [],
                "error" => "Error 1",
                "message" => "Ocurrió un Error"
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $year = Carbon::now()->format('Y');

        //$request->code = "";
        if ($request->code == null || Str::length($request->code) <= 0 || $request->code == "") {
            try {
                
                $quote = new Quote();
                $quote->id_currency = $request->id_currency;
                $quote->exchange_rate = $request->exchange_rate;
                $quote->code = "";
                $quote->reference = $request->reference;
                $quote->quote_date = $request->quote_date;
                $quote->has_igv = $request->has_igv;
                $quote->igv = $request->igv;
                $quote->aduana = $request->aduana;
                $quote->insurance = $request->insurance;
                $quote->total = $request->total;
                //$quote->gross_margin = $request->quote_date; //margen bruto
                $quote->id_user = $request->id_user;
                $quote->id_customer = $request->id_customer;

                $quote->save();
            } catch (Exception $e) {
                
                return ([
                    "status" => 0,
                    "data" => [],
                    "error" => $e,
                    "message" => "Ocurrió un Error"
                ]);
            }
            try {
                
                $quote->code = $year . "-" . $quote->id;
                //$quote->code = null;
                $quote->save();


                $condition = Condition::all();

                foreach ($condition as $cond) {
                    $tmp = new QuoteCondition();
                    $tmp->id_quote = $quote->id;
                    $tmp->id_condition = $cond->id;
                    $tmp->level = $cond->level;
                    $tmp->content = $cond->content;
                    $tmp->title = $cond->title;
                    $tmp->is_title = $cond->is_title;
                    $tmp->state = 1;
                    $tmp->save();
                }

                $qualityAttr = QualityAttribute::all();
                foreach ($qualityAttr as $qa) {
                    $tmp = new QualityControl();
                    $tmp->id_quote = $quote->id;
                    $tmp->id_quality_attribute = $qa->id;
                    $tmp->state = 0;
                    $tmp->save();
                }
            } catch (Exception $e) {
                
                return ([
                    "status" => 0,
                    "data" => [],
                    "error" => $e,
                    "message" => "Ocurrió un Error"
                ]);
            }

            $quote_conds = QuoteCondition::where('id_quote', '=', $quote->id)->get();
            $quality_contrs = QualityControl::where('id_quote', '=', $quote->id)
            ->join('quality_attributes', 'quality_controls.id_quality_attribute', '=', 'quality_attributes.id')
            ->select('quality_attributes.id as id_quality_attribute', 'quality_controls.id as id_quality_control', 'quality_attributes.*', 'quality_controls.*' )
            ->get();
            //$user = QualityControl::where('id_quote', '=', $quote->id)->get();
            $customer = Customer::find($quote->id_customer);
            if ($quote) {
                
                return ([
                    "status" => 1,
                    "data" => ["quote" => $quote, "quote_conditions" => $quote_conds, "quality_controls" => $quality_contrs, "customers" => $customer],
                    "error" => "",
                    "message" => ""
                ]);
            } else {
                
                return ([
                    "status" => 0,
                    "data" => [],
                    "error" => "Error 1",
                    "message" => "Ocurrió un Error"
                ]);
            }
        } else {
            
            return ([
                "status" => 0,
                "data" => [],
                "error" => "Error",
                "message" => "Ese Código ya existe"
            ]);
        }
    }

    public function search($term)
    {

        //return json_encode($term);
        $query = Quote::where('reference', 'like', '%' . $term . '%')
            ->orWhere('code', 'like', '%' . $term . '%')
            ->get();

        if ($query) {
            return ([
                "status" => 1,
                "data" => $query,
                "error" => "",
                "message" => ""
            ]);
        } else {
            return ([
                "status" => 0,
                "data" => [],
                "error" => "Error 1",
                "message" => "Ocurrió un Error"
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function show(Quote $quote)
    {
        try {
            $q = Quote::find($quote->id);

            $cust = Customer::where('id', '=', $q->id_customer)->get();
            $user = User::where('id', '=', $q->id_user)->get();

            $cond = QuoteCondition::where('id_quote', '=', $q->id)->get();
            $cont = QualityControl::where('id_quote', '=', $q->id)
                ->join('quality_attributes', 'quality_controls.id_quality_attribute', '=', 'quality_attributes.id')
                ->select('quality_attributes.id as id_quality_attributes', 'quality_attributes.*', 'quality_controls.id as id', 'quality_controls.*')
                ->get();
            $q_pr = QuoteProduct::where('id_quote', '=', $q->id)
                ->join("products as p", "quote_products.id_product", "=", "p.id")
                ->select('p.id AS id_product', 'p.*', 'quote_products.*')
                ->get();

            $q_pr->map(function ($qp) {
                $qp->quote_installations = QuoteInstallation::where('id_quote_products', '=', $qp->id)
                ->join('installation_parameters as t', 'quote_installations.id_installation_parameter', '=', 't.id')
                ->select('t.id as id_installation_parameter','quote_installations.id as id_quote_installation', 'quote_installations.*', 't.*')
                ->get();

                return $qp;
            });


            if ($q) {
                return ([
                    "status" => 1,
                    "data" => $q,
                    "dataAll" => [
                        "quote" => $q,
                        "quote_products" => $q_pr,
                        "quote_conditions" => $cond,
                        "quote_controls" => $cont,
                        "customer" => $cust,
                        "user" => $user
                    ],
                    "error" => "",
                    "message" => ""
                ]);
            } else {
                return ([
                    "status" => 0,
                    "data" => [],
                    "error" => "Error 1",
                    "message" => "Ocurrió un Error"
                ]);
            }
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => $e,
                "message" => "$e"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Quote $quote)
    {
        try {
            /* $quote->id_currency = $request->id_currency;
            $quote->exchange_rate = $request->exchange_rate;
            $quote->reference = $request->reference;
            $quote->quote_date = $request->quote_date;
            $quote->has_igv = $request->has_igv;
            $quote->igv = $request->igv;
            $quote->aduana = $request->aduana;
            $quote->insurance = $request->insurance;
            $quote->total = $request->total;
            //$quote->gross_margin = $request->quote_date; //margen bruto
            $quote->id_user = $request->id_user;
            $quote->id_customer = $request->id_customer;

            $quote->save(); */

            $quote->update($request->all());
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => $e,
                "message" => "Ocurrió un Error"
            ]);
        }
        $custome = Customer::where('id', '=', $quote->id_customer)->get();

        if ($quote) {
            return ([
                "status" => 1,
                "data" => ["quote" => $quote, "customer" => $custome],
                "error" => "",
                "message" => ""
            ]);
        } else {
            return ([
                "status" => 0,
                "data" => [],
                "error" => "Error 1",
                "message" => "Ocurrió un Error"
            ]);
        }
    }

    public function copy($id)
    {

        try {
            DB::beginTransaction();
            $quote_ = Quote::where('id', '=', $id)->first();
            $quoteCopy = $quote_->replicate();
            $quoteCopy->save();

            //echo $quoteCopy;

            $quote_prod = QuoteProduct::where('id_quote', '=', $id)->get();
            if (!is_null($quote_prod)) {
                foreach ($quote_prod as $qp) {
                    $md = QuoteProduct::where("id", "=", $qp->id)->first();
                    $tmp = $md->replicate();
                    $tmp->id_quote = $quoteCopy->id;
                    $tmp->save();

                    $quote_inst_para = QuoteInstallation::where('id_quote_products', '=', $md->id)->get();
                    if (!is_null($quote_inst_para)) {
                        foreach ($quote_inst_para as $qip) {
                            $md2 = QuoteInstallation::where("id", "=", $qip->id)->first();
                            $tmp2 = $md2->replicate();
                            $tmp2->id_quote_products = $tmp->id;
                            $tmp2->save();
                        }
                    }
                }
            }


            $quote_cond = QuoteCondition::where('id_quote', '=', $id)->get();
            //echo $quote_cond . "\n-----------\n";
            if (!is_null($quote_cond)) {
                foreach ($quote_cond as $qc) {
                    $md = QuoteCondition::where("id", "=", $qc->id)->first();

                    $tmp = $md->replicate();

                    $tmp->id_quote = $quoteCopy->id;

                    $tmp->save();
                }
            }

            $quality_cont = QualityControl::where('id_quote', '=', $id)->get();
            if (!is_null($quality_cont)) {
                foreach ($quality_cont as $qct) {
                    $md = QualityControl::where("id", "=", $qct->id)->first();
                    $tmp = $md->replicate();
                    $tmp->id_quote = $quoteCopy->id;
                    $tmp->save();
                }
            }


            DB::commit();
            $tqpro = QuoteProduct::where('id_quote', '=', $id)->get();
            $tqcnd = QuoteCondition::where('id_quote', '=', $id)->get();
            $tqctr = QualityControl::where('id_quote', '=', $id)->get();

            return ([
                "status" => 1,
                "data" => ["quote" => $quoteCopy, "quote_products" => $tqpro, "quote_conditions" => $tqcnd, "quality_controls" => $tqctr],
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            DB::rollBack();
            return ([
                "status" => 0,
                "data" => [],
                "error" => $e,
                "message" => "Ocurrió un Error -" . $e
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Quote  $quote
     * @return \Illuminate\Http\Response
     */
    public function destroy(Quote $quote)
    {
        //
    }
}
