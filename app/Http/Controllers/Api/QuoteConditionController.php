<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\QuoteCondition;
use Exception;
use Illuminate\Http\Request;

class QuoteConditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = QuoteCondition::all();
            return ([
                "status" => 1,
                "data" => $data,
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => $e,
                "message" => "Error al traer a lista de Condiciones"
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QuoteCondition  $quoteCondition
     * @return \Illuminate\Http\Response
     */
    public function show(QuoteCondition $quoteCondition)
    {
        try {
            $data = QuoteCondition::find($quoteCondition);
            return ([
                "status" => 1,
                "data" => $data,
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => $e,
                "message" => "Error al traer a lista de Condiciones"
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QuoteCondition  $quoteCondition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuoteCondition $quoteCondition)
    {
        try {
            /* $quoteCondition->state = $request->state;
            $quoteCondition->content = $request->content;
            $quoteCondition->save(); */

            $quoteCondition->update($request->all());

            return ([
                "status" => 1,
                "data" => $quoteCondition,
                "error" => "",
                "message" => "Se actualizó correctamente"
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => $e,
                "message" => "Error al actualizar la condición"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuoteCondition  $quoteCondition
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuoteCondition $quoteCondition)
    {
        //
    }
}
