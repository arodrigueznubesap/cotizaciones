<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Parameter;
use App\Models\Quote;
use App\Models\QuoteInstallation;
use App\Models\QuoteProduct;
use Exception;
use Illuminate\Http\Request;

class QuoteInstallationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = QuoteInstallation::all();
        return ([
            "status" => 1,
            "data" => $data,
            "error" => "",
            "message" => ""
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QuoteInstallationParameter  $quoteProductParameter
     * @return \Illuminate\Http\Response
     */
    public function show(QuoteInstallation $quoteInstallation)
    {
        try {
            $data = QuoteInstallation::where("quote_installations.id", '=', $quoteInstallation->id)
                ->join('installation_parameters as i', 'quote_installations.id_installation_parameter', '=', 'i.id')
                ->get([
                    "quote_installations.id",
                    "quote_installations.id_quote_products",
                    "quote_installations.id_installation_parameter",
                    "i.level",
                    "i.description",
                    "i.unit",
                    "quote_installations.quantity",
                    "quote_installations.days",
                    "quote_installations.dol_x_unit",
                    "quote_installations.sol_x_unit",
                    "quote_installations.dollar_total",
                    "quote_installations.sol_total",
                    "quote_installations.created_at",
                    "quote_installations.updated_at",
                ]);
                

            return ([
                "status" => 1,
                "data" => $data,
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => $e,
                "message" => ""
            ]);
        }
    }

    public function search($id_quote_product)
    {
        $data = QuoteInstallation::join('quote_products as qp', 'quote_installations.id_quote_products', '=', 'qp.id')
            ->where('qp.id_quote', '=', $id_quote_product)
            ->get();
        $temp = [];

        $all = $data->map(function ($dt) {

            if ($dt->days > 0) {

                return [
                    'id_quote_installation' => $dt->id_installation_parameter,
                    'report_description' => $dt->report_description,
                    'days' => $dt->days,
                    'quantity' => $dt->quantity
                ];
            } else {
                return [];
            }
        });


        return ([
            "status" => 1,
            "data" => $all->filter(),
            "data2" => $data,
            "error" => "",
            "message" => ""
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QuoteInstallationParameter  $quoteProductParameter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuoteInstallation $quoteInstallation)
    {
        //return ["HOLA"];
        try {
            $quoteInstallation->update($request->all());


            //echo $quoteProductParameter;


            $data_ = QuoteInstallation::where("quote_installations.id", '=', $quoteInstallation->id)
                ->join('installation_parameters as i', 'quote_installations.id_installation_parameter', '=', 'i.id')
                ->get([
                    "quote_installations.id",
                    "quote_installations.id_quote_products",
                    "quote_installations.id_installation_parameter",
                    "i.level",
                    "i.description",
                    "i.unit",
                    "quote_installations.quantity",
                    "quote_installations.days",
                    "quote_installations.dol_x_unit",
                    "quote_installations.sol_x_unit",
                    "quote_installations.dollar_total",
                    "quote_installations.sol_total",
                    "quote_installations.created_at",
                    "quote_installations.updated_at",
                    "quote_installations.updated_at",
                    "quote_installations.updated_at",
                    "quote_installations.updated_at",
                    "quote_installations.updated_at"
                ])->first();



            //$igv = Parameter::where('id', '=', '2')->first();
            $cambio = Parameter::where('id', '=', '1')->first();


            //echo $data_;
            $p1 = $quoteInstallation->dollar_total;



            $quoteInstallation->sol_x_unit = $data_->dol_x_unit * $cambio->value;
            //echo "POSTDATA: ". $data_;
            $quoteInstallation->dollar_total = $data_->days * $data_->quantity * $data_->dol_x_unit;
            $quoteInstallation->sol_total = $data_->days * $data_->quantity * $data_->dol_x_unit * $cambio->value;;
            $quoteInstallation->save();


            $qp = QuoteProduct::find($quoteInstallation->id_quote_products);
            //$quot = Quote::find($qp->id_quote);
            if ($p1 == 0) {
                if ($data_->level == 1) {

                    $qp->total_inst_perso = $qp->total_inst_perso + $quoteInstallation->dollar_total;
                }
                if ($data_->level == 2) {
                    $qp->total_inst_equip = $qp->total_inst_equip + $quoteInstallation->dollar_total;
                }
                if ($data_->level == 3) {
                    $qp->total_inst_otros = $qp->total_inst_otros + $quoteInstallation->dollar_total;
                }

                $qp->total_inst = $qp->total_inst + $quoteInstallation->dollar_total;
            } else {
                if ($data_->level == 1) {

                    $qp->total_inst_perso = $qp->total_inst_perso - $p1;
                    $qp->total_inst_perso = $qp->total_inst_perso + $quoteInstallation->dollar_total;
                }
                if ($data_->level == 2) {
                    $qp->total_inst_equip = $qp->total_inst_equip - $p1;
                    $qp->total_inst_equip = $qp->total_inst_equip + $quoteInstallation->dollar_total;
                }
                if ($data_->level == 3) {
                    $qp->total_inst_otros = $qp->total_inst_otros - $p1;
                    $qp->total_inst_otros = $qp->total_inst_otros + $quoteInstallation->dollar_total;
                }
                $qp->total_inst = $qp->total_inst - $p1;
                $qp->total_inst = $qp->total_inst + $quoteInstallation->dollar_total;
            }

           /* if ($qp->quantity > 0) {
                $qp->usd_x_m2_inst =  $qp->total_inst / $qp->quantity;
            }
            else{
                $qp->usd_x_m2_inst = 0;
            }*/
            //$qp->usd_x_m2_inst = 
            $qp->save();






            //$qp = QuoteProduct::find($quoteProductParameter->id_quote_products);

            //$qu
            //foreach($qp->)


            return ([
                "status" => 1,
                "data" => [
                    'quote_installation_parameter' => $quoteInstallation,
                    'total_inst_perso' => $qp->total_inst_perso,
                    'total_inst_equip' => $qp->total_inst_equip,
                    'total_inst_otros' => $qp->total_inst_otros,
                    'total_inst' => $qp->total_inst,
                    'usd_x_m2_inst' => $qp->usd_x_m2_inst,
                    'cuadrillas' => $qp->cuadrillas,
                    'm2_day_cuadrillas' => $qp->m2_day_cuadrillas,
                    'holgura' => $qp->holgura,
                    'area_trabajo' => $qp->area_trabajo,
                    'total_days' => $qp->total_days
                ],
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => $e,
                "message" => "$e"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuoteInstallationParameter  $quoteProductParameter
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuoteInstallation $quoteInstallation)
    {
        //
    }
}
