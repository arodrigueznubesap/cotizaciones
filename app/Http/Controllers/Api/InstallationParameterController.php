<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\InstallationParameter;
use Illuminate\Http\Request;

class InstallationParameterController extends Controller //Para Parámetros de instalación
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InstallationParameter  $installationParameter
     * @return \Illuminate\Http\Response
     */
    public function show(InstallationParameter $installationParameter)
    {
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InstallationParameter  $installationParameter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InstallationParameter $installationParameter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InstallationParameter  $installationParameter
     * @return \Illuminate\Http\Response
     */
    public function destroy(InstallationParameter $installationParameter)
    {
        //
    }
}
