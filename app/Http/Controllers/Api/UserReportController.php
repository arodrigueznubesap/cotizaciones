<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Exception;

class UserReportController extends Controller
{

    public function index()
    {
        $data = User::join('quotes', 'users.id', '=', 'quotes.id_user')
            ->join('customers', 'quotes.id_customer', '=', 'customers.id')
            ->select(
                'quotes.id as id_quote',
                'users.id as id_user',
                'users.name as user',
                'customers.name as customer',
                'quotes.code',
                'quotes.quote_date',
                'quotes.reference',
                'quotes.total',

            )
            ->get();

        return ([
            "status" => 1,
            "data" => $data,
            "error" => "",
            "message" => ""
        ]);
    }

    public function userReport($user = null, $page = 1, $size = 15, $from = "1997/01/01", $to = "2099/12/31")
    {
        try {
            if ($user == "null" && ($from == "null" || $to == "null")) {
                $data2 = User::join('quotes', 'users.id', '=', 'quotes.id_user')->get();
                $total = count($data2);
                $data = User::join('quotes', 'users.id', '=', 'quotes.id_user')
                    ->join('customers', 'quotes.id_customer', '=', 'customers.id')
                    ->select(
                        'quotes.id as id_quote',
                        'users.id as id_user',
                        'users.name as user',
                        'customers.name as customer',
                        'quotes.code',
                        'quotes.quote_date',
                        'quotes.reference',
                        'quotes.total',

                    )
                    ->limit($size)
                    ->offset(($page - 1) * $size)
                    ->get();
                return ([
                    "status" => 1,
                    "data" => $data,
                    "total" => $total,
                    "error" => "",
                    "message" => ""
                ]);
            }

            if ($user != "null" && ($from == "null" || $to == "null")) {
                $data2 = User::join('quotes', 'users.id', '=', 'quotes.id_user')
                ->where('users.name', 'like', "%$user%")
                    ->get();
                $total = count($data2);
                $data = User::join('quotes', 'users.id', '=', 'quotes.id_user')
                    ->join('customers', 'quotes.id_customer', '=', 'customers.id')
                    ->where('users.name', 'like', "%$user%")
                    ->select(
                        'quotes.id as id_quote',
                        'users.id as id_user',
                        'users.name as user',
                        'customers.name as customer',
                        'quotes.code',
                        'quotes.quote_date',
                        'quotes.reference',
                        'quotes.total',

                    )
                    ->limit($size)
                    ->offset(($page - 1) * $size)
                    ->get();
                return ([
                    "status" => 1,
                    "data" => $data,
                    "total" => $total,
                    "error" => "",
                    "message" => ""
                ]);
            }
            if ($user == "null" && ($from != "null" || $to != "null")) {
                $data2 = User::join('quotes', 'users.id', '=', 'quotes.id_user')
                    ->where('quotes.quote_date', '>=', $from)
                    ->where('quotes.quote_date', '<=', $to)
                    ->get();
                $total = count($data2);
                $data = User::join('quotes', 'users.id', '=', 'quotes.id_user')
                    ->join('customers', 'quotes.id_customer', '=', 'customers.id')
                    ->where('quotes.quote_date', '>=', $from)
                    ->where('quotes.quote_date', '<=', $to)
                    ->select(
                        'quotes.id as id_quote',
                        'users.id as id_user',
                        'users.name as user',
                        'customers.name as customer',
                        'quotes.code',
                        'quotes.quote_date',
                        'quotes.reference',
                        'quotes.total',

                    )
                    ->limit($size)
                    ->offset(($page - 1) * $size)
                    ->get();
                return ([
                    "status" => 1,
                    "data" => $data,
                    "total" => $total,
                    "error" => "",
                    "message" => ""
                ]);
            }

            if ($user != "null" && ($from != "null" || $to != "null")) {
                $data2 = User::join('quotes', 'users.id', '=', 'quotes.id_user')
                    ->where('quotes.quote_date', '>=', $from)
                    ->where('quotes.quote_date', '<=', $to)
                    ->where('users.name', 'like', "%$user%")
                    ->get();
                $total = count($data2);
                $data = User::join('quotes', 'users.id', '=', 'quotes.id_user')
                    ->join('customers', 'quotes.id_customer', '=', 'customers.id')
                    ->where('quotes.quote_date', '>=', $from)
                    ->where('quotes.quote_date', '<=', $to)
                    ->where('users.name', 'like', "%$user%")
                    ->select(
                        'quotes.id as id_quote',
                        'users.id as id_user',
                        'users.name as user',
                        'customers.name as customer',
                        'quotes.code',
                        'quotes.quote_date',
                        'quotes.reference',
                        'quotes.total',

                    )
                    ->limit($size)
                    ->offset(($page - 1) * $size)
                    ->get();
                return ([
                    "status" => 1,
                    "data" => $data,
                    "total" => $total,
                    "error" => "",
                    "message" => ""
                ]);
            }
        } catch (Exception $e) {
            return ([
                "status" => 1,
                "data" => [],
                "error" => "$e",
                "message" => "$e"
            ]);
        }
    }

    public function search($id_user)
    {
        //$data = User::where('id')
    }
}
