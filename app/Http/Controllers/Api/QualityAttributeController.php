<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\QualityAttribute;
use Illuminate\Http\Request;

class QualityAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QualityAttribute  $qualityAttribute
     * @return \Illuminate\Http\Response
     */
    public function show(QualityAttribute $qualityAttribute)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QualityAttribute  $qualityAttribute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QualityAttribute $qualityAttribute)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QualityAttribute  $qualityAttribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(QualityAttribute $qualityAttribute)
    {
        //
    }
}
