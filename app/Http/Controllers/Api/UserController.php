<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;



class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = User::all();



            return ([
                "status" => 1,
                "data" => $data,
                "error" => "",

                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 1,
                "data" => [],
                "error" => "$e",
                "message" => "$e"
            ]);
        }
    }

    public function list($page)
    {
        try {
            $data2 = User::all();
            $total = count($data2);

            $data = User::where('id', '>', 0)->limit(15)->offset(($page - 1) * 15)->get();
            return ([
                "status" => 1,
                "data" => $data,
                "total" => $total,
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 1,
                "data" => [],
                "error" => "$e",
                "message" => "$e"
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        try {
            $user->update($request->all());
            return ([
                "status" => 1,
                "data" =>  $user,
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" =>  [],
                "error" => $e,
                "message" => "$e"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    /*******************************/

    public function register(Request $request)
    {

        try {
            $request->validate([
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required'
            ]);

            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);

            $user->save();


            return response()->json([
                "status" => 1,
                "data" => $user,
                "msg" => "Registro Exitoso"
            ]);
        } catch (Exception $e) {
            return response()->json([
                "status" => 0,
                "data" => [],
                "msg" => "$e"
            ]);
        }
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = User::where("email", "=", $request->email)->first();

        if (isset($user->id)) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken("auth_token")->plainTextToken;
                return response()->json([
                    "status" => 1,
                    "msg" => "Acceso Correcto!!",
                    "access_token" => $token,
                    "id_user" => $user->id_user,
                    "name" => $user->name,
                    "email" => $user->email,
                    "user" => $user
                ]);
            } else {
                return response()->json([
                    "status" => 0,
                    "msg" => "El password es incorrecto!!",
                ], 404);
            }
        } else {
            return response()->json([
                "status" => 0,
                "msg" => "Usuario no registrado!!",
            ], 404);
        }
    }
    public function userProfile()
    {
        return response()->json([
            "status" => 1,
            "msg" => "Acerca del perfil de usuario",
            "data" => auth()->user()
        ]);
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();
        return response()->json([
            "status" => 0,
            "msg" => "Salió del Sistema"
        ]);
    }
}
