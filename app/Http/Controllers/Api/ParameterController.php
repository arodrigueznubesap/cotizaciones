<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Parameter;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ParameterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $date = Carbon::now()->format('Y-m-d');
        try {
            $data = Parameter::all();
            return ([
                "status" => 1,
                "data" => $data,
                "today" => $date,
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => "$e",
                "message" => ""
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Parameter  $parameter
     * @return \Illuminate\Http\Response
     */
    public function show(Parameter $parameter)
    {
        try {

            $data = Parameter::all();
            return ([
                "status" => 1,
                "data" => $data,
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => "$e",
                "message" => ""
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Parameter  $parameter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Parameter $parameter)
    {
        try {
            $parameter->update($request->all());

            return ([
                "status" => 1,
                "data" => $parameter,
                "error" => "",
                "message" => ""
            ]);
        } catch (Exception $e) {
            return ([
                "status" => 0,
                "data" => [],
                "error" => "$e",
                "message" => ""
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Parameter  $parameter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Parameter $parameter)
    {
        //
    }
}
