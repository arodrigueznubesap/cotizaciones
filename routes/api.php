<?php

use App\Http\Controllers\Api\CustomerController;
use App\Http\Controllers\Api\ParameterController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\QualityControlController;
use App\Http\Controllers\Api\QuoteConditionController;
use App\Http\Controllers\Api\QuoteController;
use App\Http\Controllers\Api\QuoteInstallationController;
use App\Http\Controllers\Api\QuoteProductController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\UserReportController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [UserController::class, 'register']);
Route::post('log-in', [UserController::class, 'login']);
Route::get('/Unauthorized', function () {
    return "No Autorizado!!";
})->name('login');


Route::group(['middleware' => ["auth:sanctum"]], function () {
    Route::get('user-profile', [UserController::class, 'userProfile']);
    Route::get('logout', [UserController::class, 'logout']);
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('user/list/{page}', [UserController::class, 'list'])->name('listU')->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});
Route::put('user/update/{user}', [UserController::class, 'update'])->name('user.update')->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});



//Route::group(['middleware' => ["auth:sanctum"]], function () {
Route::get('customer/search/{term}', [CustomerController::class, 'search'])->name('customer.search');
Route::resource('customer', CustomerController::class)->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});

Route::get('quote/search/{term}', [QuoteController::class, 'search'])->name('quote.search')->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});
Route::get('quote/copy/{id}', [QuoteController::class, 'copy'])->name('quote.copy')->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});
Route::resource('quote', QuoteController::class)->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});

Route::resource('product', ProductController::class)->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});

Route::get('product/list/{page}', [ProductController::class, 'list'])->name('list')->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});

Route::resource('quote_product', QuoteProductController::class)->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});

Route::get('quote_product/search/{id}', [QuoteProductController::class, 'search'])->name('search')->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});


Route::get('quote_product/installation/{id_quote}', [QuoteProductController::class, 'installationParams'])->name('quote_product.installationParams')->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});

/*Route::put('quote_installation/{quote_installation}', [QuoteInstallationController::class, 'update'])->name('quote_installation.update')->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});*/


Route::resource('quote_installation', QuoteInstallationController::class)->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});

Route::get('quote_installation/search/{id}', [QuoteInstallationController::class, 'search'])->name('quote_installation_parameter.search')->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});

Route::resource('quality_control', QualityControlController::class)->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});


Route::get('user_report', [UserReportController::class, 'index'])->name('user_report.index')->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});

Route::get('user_report/user/{user}/page/{page}/{size}/date/{from}/{to}', [UserReportController::class, 'userReport'])->name('user_report.userReport')->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});

Route::resource('parameter', ParameterController::class)->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});

Route::resource('quote_condition', QuoteConditionController::class)->missing(function () {
    return  Response()->json(["status" => "0", "data" => [], "error" => "ERROR", "message" => "No se encontró"], $status = 202, $header = ['content-type', 'application/json']);
});



//});
