<?php
    include("fpdf184/fpdf.php");
    require("conversor.php");


    $quoteId = $_GET["id"];

    $quote = json_decode(file_get_contents("https://ccallpa-api.nubesap.net.pe/api/quote/$quoteId"));
    //$quote = json_decode(file_get_contents("http://cotizaciones.test/api/quote/$quoteId"));
    //$f = fopen('quote.txt','w'); fwrite($f,print_r($quote,true));

    if(empty($quote->dataAll)){
        return;
    }
    $quote_installation = json_decode(file_get_contents("https://ccallpa-api.nubesap.net.pe/api/quote_product/installation/$quoteId"));
    //$f = fopen('quote_installation.txt','w'); fwrite($f,print_r($quote_installation,true));

    $quote_installation = $quote_installation->data;
    //$customerId = $quote->data->id_customer;

    //$f = fopen('prueba2.txt','w'); fwrite($f,print_r($customerId,true));

    $customer = $quote->dataAll->customer;
    //$f = fopen('customer.txt','w'); fwrite($f,print_r($customer,true));

    //$quote_products = json_decode(file_get_contents("http://quotes.cotizaciones.nubesap.net.pe/api/quote_product/$quoteId"));
    $quote_products = $quote->dataAll->quote_products;
    $quote_conditions = $quote->dataAll->quote_conditions;
    $quote_controls = $quote->dataAll->quote_controls;
    $quote_user = $quote->dataAll->user;
        
    $cols_header = array('ID', 'DESCRIPCIÓN', 'UNID.', 'CANT.', 'P.UNIT.', 'TOTAL');
    $cols = array(10, 105, 15, 20, 20, 20);
    $cols_title = array(10, 180);
    $cols_footer = array(150, 20, 20);
    
    $customername = $customer[0]->name;
    $customeremail = $customer[0]->email;
    $quotecode = $quote->data->code;
    $quotedate = $quote->data->quote_date;
    $username = $quote_user[0]->name;
    $usermovil = $quote_user[0]->movil;
    $useremail = $quote_user[0]->email;

    if($quote->data->id_currency == 1){
        $quote_currency = ' SOLES';
        $currency_symbol = 'S/. ';
    }
    if($quote->data->id_currency == 2){
        $quote_currency = ' DOLARES';
        $currency_symbol = '$ ';
    }

    class PDF extends FPDF
    {
    // Cabecera de página
        function Header()
        {
            // Logo
            $this->Image('cabecera_cotizacion.png',0,0,210,57,'PNG');
            $this->Image('logo.png',10,0,80,30,'PNG');
            $this->SetXY(130,20);
            $this->SetFont('times','',36);
            $this->SetTextColor(213,213,213);
            $this->Cell(0,0,utf8_decode("COTIZACIÓN"),0,2,"L");
            $this->SetFont('arial','B',12);
            $this->SetTextColor(255,255,255);
            $this->SetXY(10,37);
            $this->Cell(0,7,utf8_decode("CLIENTE   : ".$GLOBALS["customername"]),0,2,"L");
            $this->Cell(0,7,utf8_decode("CORREO   : ".$GLOBALS["customeremail"]),0,2,"L");
            $this->SetFont('arial','B',10);
            $this->SetTextColor(213,213,213);
            $this->SetXY(135,37);
            $this->Cell(20,5,utf8_decode("COT-N°  : "),0,0,"L");
            $this->Cell(30,5,utf8_decode($GLOBALS["quotecode"]),0,0,"R");
            $this->Ln();
            $this->SetX(135);
            $this->Cell(20,5,utf8_decode("FECHA   : "),0,0,"L");
            $this->Cell(30,5,utf8_decode($GLOBALS["quotedate"]),0,0,"R");
        }
        // Pie de página
        function Footer()
        {
            // Posición: a 1,5 cm del final
            //$this->SetY(-50);
            $this->Image('pie_cotizacion.png',0,262,210,35,'PNG');
            // Arial italic 8
            $this->SetFont('Arial','B',10);
            $this->SetTextColor(255,255,255);
            // Número de página
            $this->SetXY(50,-30);
            $this->Cell(0,10,utf8_decode('Página - ').$this->PageNo().'/{nb}',0,0,'L');
            $this->SetFont('Arial','B',10);
            //$this->SetTextColor(0,0,0);
            $this->SetY(-30);
            $this->SetX(135);
            $this->Cell(0,5,utf8_decode($GLOBALS["username"]),0,1,'L');
            $this->SetX(135);
            $this->Cell(0,5,'TELEF.: '.utf8_decode($GLOBALS["usermovil"]),0,1,'L');
            $this->SetX(135);
            $this->Cell(0,5,'EMAIL : '.utf8_decode($GLOBALS["useremail"]),0,1,'L');
            $this->SetY(-12);
            $this->Cell(0,10,utf8_decode('CCALLPA SAC RUC 20454029918 / Dirección: Av. Mario Vargas Llosa 334  Arequipa - Arequipa /  Telef. '),0,0,'C');
        }
    }

    //----------------------------//
    $pdf = new PDF();
    $pdf->AliasNbPages();
    $pdf->AddPage();
    //$pdf->SetMargins(2,80,2);
    /* //NUEVA FUENTE
    $pdf->AddFont('Square','','square.php');
    */ //FIN NUEVA FUENTE 
    $alto = $pdf->GetPageHeight();//297
    $ancho = $pdf->GetPageWidth();//210
    //----------------------------//
    //$pdf->Image('cabecera_cotizacion.png',0,0,210,57,'PNG');
    //$pdf->Image('pie_cotizacion.png',0,262,210,35,'PNG');
    //----------------------------//
    /*
    $pdf->SetXY(130,20);
    $pdf->SetFont('times','',36);
    $pdf->SetTextColor(213,213,213);
    $pdf->Cell(0,0,utf8_decode("COTIZACIÓN"),0,2,"L");
    */
    //$pdf->SetFont('Arial','',8);
    //----------------------------//
    /*
    $customername = $customer[0]->name;
    $customeremail = $customer[0]->email;
    */
    //----------------------------//
    /*
    $pdf->SetFont('arial','B',10);
    $pdf->SetTextColor(213,213,213);
    $pdf->SetXY(135,37);
    $pdf->Cell(20,5,utf8_decode("COT-N°  : "),0,0,"L");
    $pdf->Cell(30,5,utf8_decode($quote->data->code),0,0,"R");
    $pdf->Ln();
    $pdf->SetX(135);
    $pdf->Cell(20,5,utf8_decode("FECHA   : "),0,0,"L");
    $pdf->Cell(30,5,utf8_decode($quote->data->quote_date),0,0,"R");
    */
    //----------------------------//
    $pdf->SetXY(10,55);
    $pdf->SetFont('arial','',12);
    $pdf->SetTextColor(50,78,110);
    $pdf->Multicell(0,8,"REFERENCIA : ".$quote->data->reference,0,"J");
    $pdf->Ln();
    //----------------------------//
    $pdf->SetX(10);
    $pdf->SetFont('arial','',10);
    $pdf->Cell(0,5,utf8_decode("Estimado(s) señor(es) :"),0,2,"L");
    $pdf->SetX(10);
    $pdf->Cell(0,5,utf8_decode("Mediante el presente, les hacemos llegar nuestra propuesta económica por lo siguiente:"),0,2,"L");
    //----------------------------//
    $pdf->SetFillColor(255,255,255);
    $pdf->SetTextColor(0,96,171);
    $pdf->SetDrawColor(50,78,110);
    $pdf->SetLineWidth(.2);
    $pdf->SetFont('arial','B',8);
    /////// Inicio Cabecera Tabla///////
    $pdf->Ln();
    $pdf->SetX(10);
    for($i=0;$i<count($cols_header);$i++)
        $pdf->Cell($cols[$i],10,utf8_decode($cols_header[$i]),0,0,'C',false);
    $pdf->Ln();
    /////// Fin Cabecera Tabla///////
    //----------------------------//
    /////// Inicio Detalle ///////
    $pdf->SetFont('arial','',8);
    $pdf->SetTextColor(50,78,110);
    $pdf->SetLineWidth(.2);
    $AuxX = $pdf->GetX();
    $AuxY = $pdf->GetY();
    $AuxY_Obs = $pdf->GetY();
    $Sw_Obs = false;
    $line_h = 8;
    //$pdf->Line(12,$AuxY,200,$AuxY);
    
    foreach($quote_products as $item){
        if($Sw_Obs){
            $pdf->SetY($AuxY_Obs);
            $AuxY = $AuxY_Obs;
            $Sw_Obs = false;
        }
        if($item->is_title){
            $AuxY = $pdf->GetY();
            //$pdf->Cell($cols_title[0],8,/*$item->level*/"",0,0,'C',false);
            $pdf->Cell($cols_title[0],$line_h,$item->level*1,0,0,'L',false);
            $pdf->Cell($cols_title[1],$line_h,($item->title),0,0,'C',false);
            $pdf->Line(10,$AuxY,200,$AuxY);
            $pdf->Ln($line_h);
        }else{
            //$pdf->Cell($cols[0],8,"",0,0,'C',false);
            $pdf->Cell($cols[0],$line_h,$item->level*1,0,0,'L',false);
            //$pdf->Cell($cols[1],10,utf8_decode($item->description)."\n"."otro ontenido",'B',0,'C',true);
            $AuxY = $pdf->GetY();
            $AuxX = $pdf->GetX();
            $pdf->MultiCell($cols[1],$line_h,utf8_decode($item->description)."\n".utf8_decode($item->observation),0,'J',false);
            if(!empty($item->observation)){
                //$pdf->Ln();
                $AuxY_Obs = $pdf->GetY();
                $Sw_Obs =  true;
            }else{
                //$AuxY = $AuxY_Obs;
            }
            $pdf->SetXY($AuxX+$cols[1],$AuxY);
            $pdf->Cell($cols[2],$line_h,$item->unit,0,0,'C',false);
            $pdf->Cell($cols[3],$line_h,number_format($item->quantity,2,'.',','),0,0,'C',false);
            $pdf->Cell($cols[4],$line_h,number_format($item->unit_price,2,'.',','),0,0,'C',false);
            $pdf->Cell($cols[5],$line_h,number_format($item->subtotal,2,'.',','),0,0,'C',false);
            $pdf->Line(10,$AuxY,200,$AuxY);
            $pdf->Ln();
        }
        if($pdf->GetY()>=230){
            //$pdf->Write(5,'linea:'.$pdf->GetY());
            //$pdf->Write(5,'cantidad:'.count($quote_products));
            $pdf->AddPage();
            $pdf->SetXY(10,55);
        }
    }
    if($Sw_Obs){
        $pdf->SetY($AuxY_Obs);
        $AuxY = $pdf->GetY();
        $Sw_Obs = false;
    }
    
    //$pdf->Line(12,$AuxY,200,$AuxY);
    $pdf->SetFont('Arial','',7);
    $AuxY = $pdf->GetY();
    $AuxX = $pdf->GetX();
    $pdf->Ln(6);
    $pdf->MultiCell($cols_footer[0]-10,6,'SON: '.convertir($quote->data->total).$quote_currency,0,'L',false);
    $pdf->SetXY($AuxX+$cols_footer[0],$AuxY);
    $pdf->SetFont('Arial','B',8);
    $pdf->Cell($cols_footer[1],6,'SUBTOTAL',0,0,'L',false);
    $pdf->Cell($cols_footer[2],6,$currency_symbol.number_format(($quote->data->subtotal_general),2,'.',','),0,0,'R',false);
    $pdf->Ln();
    $pdf->SetX($AuxX+$cols_footer[0]);
    $pdf->Cell($cols_footer[1],6,'IGV '.$quote->data->igv_rate,0,0,'L',false);
    $pdf->Cell($cols_footer[2],6,$currency_symbol.number_format($quote->data->igv,2,'.',','),0,0,'R',false);
    $pdf->Ln();
    $pdf->SetX($AuxX+$cols_footer[0]);
    $pdf->Cell($cols_footer[1],6,'TOTAL',0,0,'L',false);
    $pdf->Cell($cols_footer[2],6,$currency_symbol.number_format($quote->data->total,2,'.',','),0,0,'R',false);
    $pdf->SetFont('Arial','',8);
    $pdf->Ln(10);
    $pdf->Line(10,$AuxY,200,$AuxY);
    /////// Fin Detalle ///////
/*
   
    // Línea de cierre
    $pdf->Cell(array_sum($cols),0,'','T');
    $pdf->Ln(2);
  */  

    /////// Inicio Condiciones ///////
    
    // COMPLETAR
    $pdf->SetFont('Arial','',8);
    //$sw_title_obligations = true;
    //$sw_title_scope = true;
    //$sw_title_terms = true;
    //$sw_title_quality = true;
    //$sw_title_staff = true;
    //$sw_title_hardware = true;
    //$sw_installation = true;
    //$sw_quality = true;
/*
    if($pdf->GetY()>=110 ){
        $pdf->AddPage();
        $pdf->SetXY(10,55);
    }
    */
    if($pdf->GetY()>=240){
        //$pdf->Write(5,'linea:'.$pdf->GetY());
        //$pdf->Write(5,'cantidad:'.count($quote_products));
        $pdf->AddPage();
        $pdf->SetXY(10,55);
    }
    $FontSize = 8;
    $Col2 = true;
    $AuxY = $pdf->GetY();
    $AuxY_Col1 = $pdf->GetY();
    $AuxY_Col2 = $pdf->GetY();
    $SetMarco = false;
    foreach ($quote_conditions as $condition)
    {
        if(intval($condition->level) == 1){
            if($condition->state){
                $pdf->SetFont('Arial','BU',$FontSize);
                $pdf->Cell(0,6,utf8_decode($condition->title),0,1,'L');
                $pdf->SetFont('Arial','',$FontSize);
                $pdf->Multicell(0,4,utf8_decode($condition->content),0,'L');
                $pdf->Ln(2);
                //$AuxY = $pdf->GetY();
                $pdf->AddPage();
                $pdf->SetXY(10,55);
                $AuxY = $pdf->GetY();
                $AuxY_Col1 = $pdf->GetY();
                $AuxY_Col2 = $pdf->GetY();
                /*
                if($condition->is_title){
                    $pdf->SetFont('Arial','BU',$FontSize);
                    $pdf->Cell(90,6,utf8_decode($condition->title),0,2,'L');
                }else{
                    $pdf->SetFont('Arial','',$FontSize);
                    $pdf->Multicell(90,4,utf8_decode($condition->content),0,'L');
                    $pdf->Ln(2);
                }
                */
            }
        }
        if(intval($condition->level) == 2){
            if($condition->state){
                /*
                if($condition->is_title){
                    $pdf->SetFont('Arial','BU',$FontSize);
                    $pdf->Cell(90,6,utf8_decode($condition->title),0,2,'L');
                }else{
                    $pdf->SetFont('Arial','',$FontSize);
                    $pdf->Multicell(90,4,utf8_decode($condition->content),0,'L');
                    $pdf->Ln(2);
                }
                */
                $pdf->SetFont('Arial','BU',$FontSize);
                $pdf->Cell(90,6,utf8_decode($condition->title),0,2,'L');
                $pdf->SetFont('Arial','',$FontSize);
                if(!empty($condition->content)){
                    $pdf->Multicell(90,4,utf8_decode($condition->content),0,'L');
                    $pdf->Ln(2); 
                }
            }
            $AuxY_Col1 = $pdf->GetY();
        }
        if(intval($condition->level) == 3){
            if($condition->state){
                if($Col2){
                    $pdf->SetY($AuxY);
                    $Col2 =false;
                }
                if($condition->is_title){
                    //$pdf->Ln(2);
                    $pdf->SetFont('Arial','BU',$FontSize);
                    $pdf->SetX(110);
                    $pdf->Cell(90,6,utf8_decode($condition->title),0,1,'L');
                    $AuxY_Col2 = $pdf->GetY();
                }
                if($condition->level==3.1){
                    if(!empty($quote_installation)){
                        $pdf->SetFont('Arial','BU',$FontSize);
                        $pdf->SetX(110);
                        $pdf->Cell(90,6,utf8_decode('Los Equipos'),0,2,'L');
                        foreach($quote_installation as $item){
                            if($item->level_class==2){
                                $pdf->SetFont('Arial','',$FontSize);
                                /*
                                $pdf->cell(95,5,utf8_decode($item->report_description),0,2,'L');
                                $pdf->cell(95,5,utf8_decode($item->suma),0,2,'L');
                                */
                                $pdf->SetX(110);
                                $pdf->cell(75,4,utf8_decode($item->report_description),0,0,'L');
                                $pdf->cell(15,4,utf8_decode($item->suma),0,1,'L');
                            }
                        }
                        $pdf->Ln(2);
                        $AuxY_Col2 = $pdf->GetY();
                    }
                }
                if($condition->level==3.2){
                    if(!empty($quote_installation)){
                        $pdf->SetFont('Arial','BU',$FontSize);
                        $pdf->SetX(110);
                        $pdf->Cell(90,6,utf8_decode('El Personal'),0,2,'L');
                        foreach($quote_installation as $item){
                            if($item->level_class==2){
                                $pdf->SetFont('Arial','',$FontSize);
                                /*
                                $pdf->cell(95,5,utf8_decode($item->report_description),0,2,'L');
                                $pdf->cell(95,5,utf8_decode($item->suma),0,2,'L');
                                */
                                $pdf->SetX(110);
                                $pdf->cell(75,4,utf8_decode($item->report_description),0,0,'L');
                                $pdf->cell(15,4,utf8_decode($item->suma),0,1,'L');
                            }
                        }
                        $pdf->Ln(2);
                        $AuxY_Col2 = $pdf->GetY();
                    }
                }
                if($condition->level==3.3){
                    if(!empty($quote_installation)){
                        $pdf->SetY($AuxY_Col1);
                        $pdf->SetFont('Arial','BU',$FontSize);
                        $pdf->SetX(10);
                        $pdf->Cell(90,6,utf8_decode('CONTROL DE CALIDAD'),0,2,'L');
                        foreach($quote_controls as $control){
                            if($control->state){
                                $pdf->SetFont('Arial','',$FontSize);
                                $pdf->Multicell(90,4,utf8_decode($control->description),0,'L');
                            }
                        }
                        $pdf->Ln(2);
                        //$AuxY_Col2 = $pdf->GetY();
                    }
                }
                if($condition->level==3.4){
                    $pdf->SetY($AuxY_Col2);
                    $pdf->SetFont('Arial','BU',$FontSize);
                    $pdf->SetX(110);
                    $pdf->Cell(90,4,utf8_decode($condition->title),0,2,'L');
                    $pdf->SetFont('Arial','',$FontSize);
                    $pdf->Multicell(90,4,utf8_decode($condition->content),0,'L');
                    $pdf->Ln(2);
                }
            }
        }
        if(intval($condition->level) == 4){
            //$pdf->Write(5,$pdf->GetY());
            if($condition->state){
                $SetMarco = true;
                $FontSize = 7;
                if($condition->is_title){
                    $pdf->SetY(190);
                    $pdf->SetFont('Arial','BU',$FontSize);
                    $pdf->Cell(95,6,utf8_decode($condition->title),0,2,'L');
                }else{
                    $pdf->SetFont('Arial','',$FontSize);
                    $pdf->Cell(25,4,utf8_decode($condition->title).': ',0,0,'L');
                    $pdf->Multicell(70,4,trim(utf8_decode($condition->content)),0,'L');
                    //$pdf->Ln(2);
                }
            }
        }
    }
    if($SetMarco){
        $pdf->SetY(190);
        $pdf->Multicell(95,80,'','LTR','L',FALSE);
    }
    /*
    $pdf->Cell(0,10,'CONDICIONES',0,2,'C');
    $pdf->Cell(0,10,'CONDICIONES',0,2,'C');
    $pdf->Cell(0,10,'CONDICIONES',0,2,'C');
    $pdf->Cell(0,10,'CONDICIONES',0,2,'C');
    $pdf->Cell(0,10,'CONDICIONES',0,2,'C');
    */
    /////// Fin Condiciones ///////


    $pdf->SetFont('Arial','',8);
    $pdf->SetXY(115,230);
    $pdf->Cell(0,5,'Sin otro particular, quedamos de ustedes, atentamente.',0,2,'L');
    //$pdf->Cell(0,5,'Atentamente,',0,2,'L');
    $pdf->Ln(10);

    $pdf->Image($quote_user[0]->sign_path,150,null,33);
    //$pdf->SetLeftMargin(20);
    /*
    $pdf->SetXY(150,250);
    $pdf->Cell(30,5,utf8_decode($quote_user[0]->name),0,1,'C');
    $pdf->Cell(30,5,utf8_decode($quote_user[0]->movil),0,1,'C');
    $pdf->Cell(30,5,utf8_decode($quote_user[0]->email),0,1,'C');
    */
    
    
    $pdf->Output();