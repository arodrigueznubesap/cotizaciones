## Al clonar el Repositorio

# Primero debemos tener instalado
- Apache para PHP 8 o superior
- Mysql o MariaDB 8 o superior
- Composer

# Una vez clonado el repositorio ejecutar los siguientes comandos
- composer install
- Crear una copia del archivo .env.example y renombrarlo por .env
- php artisan key:generate
- Crear una Base de datos con el nombre db_quotes

# Actualizar los valores de configuración de la base de datos en el archivo .env
- DB_HOST = 127.0.01
- DB_PORT = 3306
- DB_DATABASE = db_quotes
- DB_USER = root
- DB_PASSWORD = 

# Realizar las migraciones
- php artisan migrate --seed