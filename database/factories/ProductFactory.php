<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        //$description = $this->faker->

        //echo $this->faker->randomElement(['Kg', 'M', 'Pza', 'Lit', 'Uni']);

        return [
            'code' => $this->faker->sentence($nbWords = 1),
            'description' => $this->faker->sentence($nbWords = 5, $variableNbWords = true),
            'unit' => $this->faker->randomElement(['Kg', 'M', 'Pza', 'Lit', 'Uni']),
            'id_product_brand' => 1,
            'id_product_type' => $this->faker->randomElement([2,3]),
            'id_installation_type' => null,
            'unit_price' => floatval($this->faker->randomNumber(2, 10.00, 1500.00))
        ];
    }
}
