<?php

namespace Database\Seeders;

use App\Models\InstallationTemplate;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $prodCero = new Product();
        $prodCero->description = "Vacio";
        $prodCero->id_product_type = 1;
        $prodCero->unit_price = "0";
        $prodCero->save();

        $prodGeo = new Product();
        $prodGeo->description = "Instalación de Geomembranas";
        $prodGeo->id_product_type = 4;
        $prodGeo->id_installation_type = 2;
        $prodGeo->unit_price = "2.25";
        $prodGeo->save();

        $prodTub = new Product();
        $prodTub->description = "Instalación de Tuberías";
        $prodTub->id_product_type = 4;
        $prodTub->id_installation_type = 3;
        $prodTub->unit_price = "2.25";
        $prodTub->save();

        $prodBan = new Product();
        $prodBan->description = "Instalación de Bandejas";
        $prodBan->id_product_type = 4;
        $prodBan->id_installation_type = 4;
        $prodBan->unit_price = "2.25";
        $prodBan->save();


    }
}
