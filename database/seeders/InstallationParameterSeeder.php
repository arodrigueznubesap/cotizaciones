<?php

namespace Database\Seeders;

use App\Models\InstallationParameter;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class InstallationParameterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $p1 = new InstallationParameter();
        $p1->description = "Técnico Mixto";
        $p1->Unit = "TEC";
        //$p1->quantity = 0;
        $p1->level = 1;
        $p1->save();

        $p2 = new InstallationParameter();
        $p2->description = "Técnico Junior";
        $p2->Unit = "TEC";
        //$p2->quantity = 0;
        $p2->level = 1;
        $p2->save();

        $p3 = new InstallationParameter();
        $p3->description = "Técnico Asistente";
        $p3->Unit = "TEC";
        //$p3->quantity = 0;
        $p3->level = 1;
        $p3->save();

        $p4 = new InstallationParameter();
        $p4->description = "Ayudantes Tentido 5 a 6";
        $p4->Unit = "AYU";
        //$p4->quantity = 0;
        $p4->level = 1;
        $p4->save();

        $p5 = new InstallationParameter();
        $p5->description = "Ing. Seguridad";
        $p5->Unit = "ING";
        //$p5->quantity = 0;
        $p5->level = 1;
        $p5->save();

        $p6 = new InstallationParameter();
        $p6->description = "Ayudante CQA";
        $p6->Unit = "AYU";
        //$p6->quantity = 0;
        $p6->level = 1;
        $p6->save();

        $p7 = new InstallationParameter();
        $p7->description = "Residente CQA";
        $p7->Unit = "RES";
        //$p7->quantity = 0;
        $p7->level = 1;
        $p7->save();

        $p8 = new InstallationParameter();
        $p8->description = "INGENIERO";
        $p8->Unit = "ING";
        //$p8->quantity = 0;
        $p8->level = 1;
        $p8->save();

        $p9 = new InstallationParameter();
        $p9->description = "Cuña";
        $p9->Unit = "HM";
        //$p9->quantity = 0;
        $p9->level = 2;
        $p9->save();

        $p10 = new InstallationParameter();
        $p10->description = "Maquina de Termofusión de 8''";
        $p10->Unit = "HM";
        //$p10->quantity = 0;
        $p10->level = 2;
        $p10->save();

        $p11 = new InstallationParameter();
        $p11->description = "Extrusora";
        $p11->Unit = "HM";
        //$p11->quantity = 0;
        $p11->level = 2;
        $p11->save();

        $p12 = new InstallationParameter();
        $p12->description = "Tensiómetro y Cuponera";
        $p12->Unit = "HM";
        //$p12->quantity = 0;
        $p12->level = 2;
        $p12->save();

        $p13 = new InstallationParameter();
        $p13->description = "Caja de Vacio";
        $p13->Unit = "HM";
        //$p13->quantity = 0;
        $p13->level = 2;
        $p13->save();

        $p14 = new InstallationParameter();
        $p14->description = "Spark Tester";
        $p14->Unit = "HM";
        //$p14->quantity = 0;
        $p14->level = 2;
        $p14->save();

        $p15 = new InstallationParameter();
        $p15->description = "Camión Grua";
        $p15->Unit = "DIA";
        //$p15->quantity = 0;
        $p15->level = 2;
        $p15->save();

        $p16 = new InstallationParameter();
        $p16->description = "Generador";
        $p16->Unit = "HM";
        //$p16->quantity = 0;
        $p16->level = 2;
        $p16->save();

        $p17 = new InstallationParameter();
        $p17->description = "Camioneta";
        $p17->Unit = "HM";
        //$p17->quantity = 0;
        $p17->level = 2;
        $p17->save();

        $p18 = new InstallationParameter();
        $p18->description = "Alimentación";
        $p18->Unit = "DIA";
        //$p18->quantity = 0;
        $p18->level = 3;
        $p18->save();

        $p19 = new InstallationParameter();
        $p19->description = "Almuerzo Ayudantes";
        $p19->Unit = "DIA";
        //$p19->quantity = 0;
        $p19->level = 3;
        $p19->save();

        $p20 = new InstallationParameter();
        $p20->description = "Alojamiento";
        $p20->Unit = "DIA";
        //$p20->quantity = 0;
        $p20->level = 3;
        $p20->save();

        $p21 = new InstallationParameter();
        $p21->description = "Pasajes Aéreos";
        $p21->Unit = "GLB";
        //$p21->quantity = 0;
        $p21->level = 3;
        $p21->save();

        $p22 = new InstallationParameter();
        $p22->description = "Traslados";
        $p22->Unit = "GLB";
        //$p22->quantity = 0;
        $p22->level = 3;
        $p22->save();

        $p23 = new InstallationParameter();
        $p23->description = "Sacos";
        $p23->Unit = "PZA";
        //$p23->quantity = 0;
        $p23->level = 3;
        $p23->save();

        $p24 = new InstallationParameter();
        $p24->description = "Cordón de Soldadura";
        $p24->Unit = "KG";
        //$p24->quantity = 0;
        $p24->level = 3;
        $p24->save();

        $p25 = new InstallationParameter();
        $p25->description = "Gasolina";
        $p25->Unit = "GLN";
        //$p25->quantity = 0;
        $p25->level = 3;
        $p25->save();
    }
}
