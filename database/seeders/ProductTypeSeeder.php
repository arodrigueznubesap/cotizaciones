<?php

namespace Database\Seeders;

use App\Models\ProductType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $productType0 = new ProductType();
        $productType0->description = "Vacio";
        $productType0->save();


        $productType1 = new ProductType();
        $productType1->description = "Producto";
        $productType1->save();

        $productType2 = new ProductType();
        $productType2->description = "Servicio";
        $productType2->save();

        $productType3 = new ProductType();
        $productType3->description = "Instalación";
        $productType3->save();
    }
}
