<?php

namespace Database\Seeders;

use App\Models\QualityAttribute;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class QualityAttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $qa1 = new QualityAttribute();
        $qa1->description = "Calibración In Situ de Equipos";
        $qa1->save();

        $qa2 = new QualityAttribute();
        $qa2->description = "Prueba de Aire";
        $qa2->save();

        $qa3 = new QualityAttribute();
        $qa3->description = "Caja de Vacío";
        $qa3->save();

        $qa4 = new QualityAttribute();
        $qa4->description = "Pruebas destructivas de Corte y Desgarre";
        $qa4->save();
    }
}
