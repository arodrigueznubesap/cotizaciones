<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new User();
        $user1->name = "Gabriel Mendoza del Solar Morán";
        $user1->email = "comerial@ccallpa.com";
        $user1->sign_path = "imgs/user_1.jpg";
        $user1->movil = "997523192";
        $user1->password = Hash::make('12345678');
        $user1->save();

        $user2 = new User();
        $user2->name = "Pedro Martin de Rivero Huamán";
        $user2->email = "comercialsur@ccallpa.com";
        $user2->sign_path = "imgs/user_2.jpg";
        $user2->movil = "967984586";
        $user2->password = Hash::make('12345678');
        $user2->save();

        $user3 = new User();
        $user3->name = "Elsa Luz Cruz Nuñez";
        $user3->email = "ventas@ccallpa.com";
        $user3->sign_path = "imgs/user_3.jpg";
        $user3->movil = "958320745 ";
        $user3->password = Hash::make('12345678');
        $user3->save();


        $user4 = new User();
        $user4->name = "Yesenia Ramos Taco";
        $user4->email = "administracion@ccallpa.com";
        $user4->sign_path = "imgs/user_4.jpg";
        $user4->movil = "951250099";
        $user4->password = Hash::make('12345678');
        $user4->save();


        $userAdmin = new User();
        $userAdmin->name = "Administrador del Sistema";
        $userAdmin->email = "admin@ccallpa.com";
        $userAdmin->password = Hash::make('12345678');
        $userAdmin->save();

    }
}
