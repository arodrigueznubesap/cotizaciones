<?php

namespace Database\Seeders;

use App\Models\Customer;
use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(ParameterSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(QualityAttributeSeeder::class);
        $this->call(ProductTypeSeeder::class);
        $this->call(ProductBrandSeeder::class);
        $this->call(InstallationTypeSeeder::class);
        $this->call(InstallationParameterSeeder::class);
        $this->call(InstallationTemplateSeeder::class);
        $this->call(ConditionSeeder::class);

        $this->call(ProductSeeder::class);
        Customer::factory(20)->create();
        Product::factory(20)->create();

        $this->call(QuoteSeeder::class);
        $this->call(QuoteProductSeeder::class);

        
    }
}
