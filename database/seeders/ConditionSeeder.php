<?php

namespace Database\Seeders;

use App\Models\Condition;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $obs = new Condition(); //Observaciones
        $obs->title = "Observaciones";
        $obs->content  = "";
        $obs->level = 1;
        $obs->save();

        $equip = new Condition(); //Obligaciones del Cliente
        $equip->title = "Obligaciones del cliente";
        $equip->content  = "";
        $equip->level = 2;
        $equip->is_title = 1;
        $equip->save();

        $equip = new Condition(); //Obligaciones del Cliente
        $equip->title = "Los Equipos";
        $equip->content  = "* Brindara un lugar seguro para el almacenamiento de equipos y materiales";
        $equip->level = 2.1;
        $equip->save();


        $perso = new Condition();
        $perso->title = "El Personal";
        $perso->content  = "* Asumirá alojamiento e hidratación del personal.";
        $perso->level = 2.2;
        $perso->save();

        $otros = new Condition();
        $otros->title = "Otros";
        $otros->content  = "* Brindará equipo para descarga y posicionamiento de rollos de geomembrana y geotextil.";
        $otros->level = 2.3;
        $otros->save();

        $equip = new Condition(); //Obligaciones del Cliente
        $equip->title = "Alcances de Nuestra Propuesta";
        $equip->content  = "";
        $equip->level = 3;
        $equip->is_title = 1;
        $equip->save();

        $equip = new Condition(); //Obligaciones del Cliente
        $equip->title = "Los Equipos";
        $equip->content  = "";
        $equip->level = 3.1;
        $equip->is_title = 0;
        $equip->save();


        $equip = new Condition(); //Obligaciones del Cliente
        $equip->title = "El Personal";
        $equip->content  = "";
        $equip->level = 3.2;
        $equip->is_title = 0;
        $equip->save();

        
        $equip = new Condition(); //Obligaciones del Cliente
        $equip->title = "Control de Calidad";
        $equip->content  = "";
        $equip->level = 3.3;
        $equip->is_title = 0;
        $equip->save();

        $alc_v = new Condition(); //Alcances Varios
        $alc_v->title = "Alcances Varios";
        $alc_v->content  = "* Los materiales de desperdicio se colocarán en una zona adyacente a la zona de trabajo.\n					
        * Toda reparación de daños causados por terceros antes de la entrega de la obra serán asumidos por el cliente.\n 					
        * Los días adicionales de instalación a causa de retrasos ocasionados por terceros serán asumidos por el cliente (US$478.00 + IGV POR DIA).\n					
        * Garantías: Instalación de geosinteticos 05 años.";
        $alc_v->level = 3.4;
        $alc_v->save();

        $equip = new Condition(); //Obligaciones del Cliente
        $equip->title = "Condiciones Comerciales";
        $equip->content  = "";
        $equip->level = 4;
        $equip->is_title = 1;
        $equip->save();

        $con_fp = new Condition(); //Condiciones Comerciales
        $con_fp->title = "Forma de Pago";
        $con_fp->content  = "Materiales 100% factura a 30 días / Instalación con valorización semanal y factura 30 días.";
        $con_fp->level = 4.1;
        $con_fp->save();

        $con_le = new Condition(); //Condiciones Comerciales
        $con_le->title = "Lugar de Entrega";
        $con_le->content  = "La Joya sobre camión.";
        $con_le->level = 4.2;
        $con_le->save();

        $con_vo = new Condition(); //Condiciones Comerciales
        $con_vo->title = "Validez de la Oferta";
        $con_vo->content  = "07 días.";
        $con_vo->level = 4.3;
        $con_vo->save();

        $con_pe = new Condition(); //Condiciones Comerciales
        $con_pe->title = "Plazo de Entrega";
        $con_pe->content  = "Geomembrana 10 días de puesta la OC.\n
        Tubería y accesorios de 10 a 15 días de puesta la OC.";
        $con_pe->level = 4.4;
        $con_pe->save();

        $con_pi = new Condition(); //Condiciones Comerciales
        $con_pi->title = "Plazo de Instalación";
        $con_pi->content  = "Estanque 01 - 15 días efectivos de recibido el terreno formalmente.\n
        Estanque 02 - 15 días efectivos de recibido el terreno formalmente.\n
        Pegas por termofusión 06 días efectivos según cronograma de obra.\n
        Relleno de zanja 08 días.\n
        Retiro de sacos 08 días.";
        $con_pi->level = 4.5;
        $con_pi->save();

    }
}
