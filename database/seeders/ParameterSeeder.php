<?php

namespace Database\Seeders;

use App\Models\Parameter;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ParameterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $obs = new Parameter(); //Observaciones
        $obs->description = "Valor de cambio";
        $obs->value  = "3.72";
        
        $obs->save();

        $obs = new Parameter(); //Observaciones
        $obs->description = "IGV";
        $obs->value  = "1.18";
        
        $obs->save();

        $obs = new Parameter(); //Observaciones
        $obs->description = "Holgura";
        $obs->value  = "6";
        
        $obs->save();
    }
}
