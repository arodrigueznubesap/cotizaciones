<?php

namespace Database\Seeders;

use App\Models\InstallationTemplate;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class InstallationTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $len_vars = 25;

        for ($i = 1; $i <= $len_vars; $i++) {

            $temp = new InstallationTemplate();
            $temp->id_installation_type = 2;
            $temp->id_installation_parameter = $i;
            $temp->save();
        }

        for ($i = 1; $i <= $len_vars; $i++) {

            $temp = new InstallationTemplate();
            $temp->id_installation_type = 3;
            $temp->id_installation_parameter = $i;
            $temp->save();
        }

        for ($i = 1; $i <= $len_vars; $i++) {

            $temp = new InstallationTemplate();
            $temp->id_installation_type = 4;
            $temp->id_installation_parameter = $i;
            $temp->save();
        }
    }
}
