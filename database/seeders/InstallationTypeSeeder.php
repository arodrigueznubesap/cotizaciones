<?php

namespace Database\Seeders;

use App\Models\InstallationType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class InstallationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $insT0 = new InstallationType();
        $insT0->description = "Null";
        $insT0->save();

        $insT1 = new InstallationType();
        $insT1->description = "Geomembrana";
        $insT1->save();

        $insT2 = new InstallationType();
        $insT2->description = "Tuberias";
        $insT2->save();

        $insT3 = new InstallationType();
        $insT3->description = "Bandejas";
        $insT3->save();
    }
}
