<?php

namespace Database\Seeders;

use App\Http\Controllers\Api\QuoteController;
use App\Models\Quote;
use Faker\Extension\Container;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;




class QuoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        $this->faker = Faker::create();

        $qt = new QuoteController();



        $req = new \Illuminate\Http\Request([

            "id_currency" => 1,
            "exchange_rate" =>  3.75,
            "reference" =>  $this->faker->paragraph(10),
            "quote_date" => '2022-04-22',
            "has_igv" => 1,
            "igv_rate" => 18.00,
            "igv" => 76.27,
            "aduana_rate" => 0.30,
            "aduana" => $this->faker->randomNumber(2, 10.00, 1500.00),
            "insurance_rate" => 0.95,
            "insurance" => $this->faker->randomNumber(2, 10.00, 1500.00),
            "total" => 500.00,
            "id_user" => 1,
            "id_customer" => 2,
            "subtotal_general" => 423.73
        ]);


        $qt->store($req);
      

        //$quote->save(),

        $req2 = new \Illuminate\Http\Request([
            "id_currency" => 1,
            "exchange_rate" =>  3.75,
            "reference" =>  $this->faker->paragraph(10),
            "quote_date" => '2022-04-22',
            "has_igv" => 1,
            "igv_rate" => 18.00,
            "igv" => 1147.63,
            "aduana_rate" => 0.30,
            "aduana" => $this->faker->randomNumber(2, 10.00, 1500.00),
            "insurance_rate" => 0.95,
            "insurance" => $this->faker->randomNumber(2, 10.00, 1500.00),
            "total" => 7523.35,
            "id_user" => 3,
            "id_customer" => 2,
            "subtotal_general" => 6375.72
        ]);


        $qt->store($req2);
    }
}
