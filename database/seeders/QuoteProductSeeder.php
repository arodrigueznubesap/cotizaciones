<?php

namespace Database\Seeders;

use App\Http\Controllers\Api\QuoteProductController;
use App\Models\QuoteProduct;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class QuoteProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->faker = Faker::create();


        /* 1 Titulo y 3 Productos */
        $qp = new QuoteProduct();
        $qp->id_quote = 1;
        $qp->level = 1;
        $qp->save();

        $qpc = new QuoteProductController();
        $qpc->update(new \Illuminate\Http\Request([
            "id_product" => 1,
            "level" => "1",
            "title" => $this->faker->paragraph(3),
            "is_title" => "1",
            "unit" => "Und",
            "quantity" => $this->faker->randomNumber(2, 1, 100),
            "unit_price" => $this->faker->randomNumber(4, 10, 5000),
            "subtotal" => $this->faker->randomNumber(5, 10, 50000),
            "costo_c_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_c_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "margen_b" => $this->faker->randomNumber(2, 0.1, 0.9),
            "pv_material" => $this->faker->randomNumber(2, 10, 500),
            "ut_x_unid" => $this->faker->randomNumber(2, 10, 500),
            "ut_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_kg_x_u" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_kg" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_dollar" => $this->faker->randomNumber(2, 10, 500)
        ]), $qp);
        /*$qpc = new QuoteProductController();
        $qpc->update(new \Illuminate\Http\Request([
            "id_product"=>1,
            "level" => "1",
            "title" => $this->faker->paragraph(3),
            "is_title" => "1",
            "unit" => "Und",
            "quantity" => $this->faker->randomNumber(2,1,100),
            "unit_price" => $this->faker->randomNumber(4,10,5000),
            "subtotal" => $this->faker->randomNumber(5,10,50000),
            "costo_c_u_dollar" => $this->faker->randomNumber(2,10,500),
            "costo_c_total_u_dollar" => $this->faker->randomNumber(2,10,500),
            "flete_dollar" => $this->faker->randomNumber(2,10,500),
            "costo_final_dollar" => $this->faker->randomNumber(2,10,500),
            "costo_final_u_dollar" => $this->faker->randomNumber(2,10,500),
            "margen_b" => $this->faker->randomNumber(2,0.1,0.9),
            "pv_material" => $this->faker->randomNumber(2,10,500),
            "ut_x_unid" => $this->faker->randomNumber(2,10,500),
            "ut_dollar" => $this->faker->randomNumber(2,10,500),
            "flete_kg_x_u" => $this->faker->randomNumber(2,10,500),
            "flete_total_kg" => $this->faker->randomNumber(2,10,500),
            "flete_total_u_dollar" => $this->faker->randomNumber(2,10,500),
            "flete_total_dollar" => $this->faker->randomNumber(2,10,500)]), $qp);*/

        $qp = new QuoteProduct();
        $qp->id_quote = 1;
        $qp->level = 1.1;
        $qp->save();

        $qpc = new QuoteProductController();
        $qpc->update(new \Illuminate\Http\Request([
            "id_product" => 8,
            "level" => "1.1",
            "title" => $this->faker->paragraph(3),
            "is_title" => "0",
            "unit" => "Und",
            "quantity" => $this->faker->randomNumber(2, 1, 100),
            "unit_price" => $this->faker->randomNumber(4, 10, 5000),
            "subtotal" => $this->faker->randomNumber(5, 10, 50000),
            "costo_c_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_c_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "margen_b" => $this->faker->randomNumber(2, 0.1, 0.9),
            "pv_material" => $this->faker->randomNumber(2, 10, 500),
            "ut_x_unid" => $this->faker->randomNumber(2, 10, 500),
            "ut_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_kg_x_u" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_kg" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_dollar" => $this->faker->randomNumber(2, 10, 500)
        ]), $qp);

        $qp = new QuoteProduct();
        $qp->id_quote = 1;
        $qp->level = 1.2;
        $qp->save();

        $qpc = new QuoteProductController();
        $qpc->update(new \Illuminate\Http\Request([
            "id_product" => 11,
            "level" => "1.2",
            "title" => $this->faker->paragraph(3),
            "is_title" => "0",
            "unit" => "Und",
            "quantity" => $this->faker->randomNumber(2, 1, 100),
            "unit_price" => $this->faker->randomNumber(4, 10, 5000),
            "subtotal" => $this->faker->randomNumber(5, 10, 50000),
            "costo_c_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_c_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "margen_b" => $this->faker->randomNumber(2, 0.1, 0.9),
            "pv_material" => $this->faker->randomNumber(2, 10, 500),
            "ut_x_unid" => $this->faker->randomNumber(2, 10, 500),
            "ut_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_kg_x_u" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_kg" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_dollar" => $this->faker->randomNumber(2, 10, 500)
        ]), $qp);

        $qp = new QuoteProduct();
        $qp->id_quote = 1;
        $qp->level = 1.3;
        $qp->save();

        $qpc = new QuoteProductController();
        $qpc->update(new \Illuminate\Http\Request([
            "id_product" => 10,
            "level" => "1.3",
            "title" => $this->faker->paragraph(3),
            "is_title" => "0",
            "unit" => "Und",
            "quantity" => $this->faker->randomNumber(2, 1, 100),
            "unit_price" => $this->faker->randomNumber(4, 10, 5000),
            "subtotal" => $this->faker->randomNumber(5, 10, 50000),
            "costo_c_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_c_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "margen_b" => $this->faker->randomNumber(2, 0.1, 0.9),
            "pv_material" => $this->faker->randomNumber(2, 10, 500),
            "ut_x_unid" => $this->faker->randomNumber(2, 10, 500),
            "ut_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_kg_x_u" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_kg" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_dollar" => $this->faker->randomNumber(2, 10, 500)
        ]), $qp);


        /*Otra Cotización con 2 titulos con 2 Productos y 2 instalaciones */

        /**Titulo */
        $qp = new QuoteProduct();
        $qp->id_quote = 2;
        $qp->level = 1;
        $qp->save();

        $qpc = new QuoteProductController();
        $qpc->update(new \Illuminate\Http\Request([
            "id_product" => 1,
            "title" => $this->faker->paragraph(3),
            "is_title" => "1",
            "unit" => "Und",
            "quantity" => $this->faker->randomNumber(2, 1, 100),
            "unit_price" => $this->faker->randomNumber(4, 10, 5000),
            "subtotal" => $this->faker->randomNumber(5, 10, 50000),
            "costo_c_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_c_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "margen_b" => $this->faker->randomNumber(2, 0.1, 0.9),
            "pv_material" => $this->faker->randomNumber(2, 10, 500),
            "ut_x_unid" => $this->faker->randomNumber(2, 10, 500),
            "ut_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_kg_x_u" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_kg" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_dollar" => $this->faker->randomNumber(2, 10, 500)
        ]), $qp);

        /**Producto 1 */
        $qp = new QuoteProduct();
        $qp->id_quote = 2;
        $qp->level = 1.1;
        $qp->save();

        $qpc = new QuoteProductController();
        $qpc->update(new \Illuminate\Http\Request([
            "id_product" => 6,
            "title" => $this->faker->paragraph(3),
            "is_title" => "0",
            "unit" => "Und",
            "quantity" => $this->faker->randomNumber(2, 1, 100),
            "unit_price" => $this->faker->randomNumber(4, 10, 5000),
            "subtotal" => $this->faker->randomNumber(5, 10, 50000),
            "costo_c_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_c_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "margen_b" => $this->faker->randomNumber(2, 0.1, 0.9),
            "pv_material" => $this->faker->randomNumber(2, 10, 500),
            "ut_x_unid" => $this->faker->randomNumber(2, 10, 500),
            "ut_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_kg_x_u" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_kg" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_dollar" => $this->faker->randomNumber(2, 10, 500)
        ]), $qp);

        /**Instalación 1 */

        $qp = new QuoteProduct();
        $qp->id_quote = 2;
        $qp->level = 1.2;
        $qp->save();

        $qpc = new QuoteProductController();
        $qpc->update(new \Illuminate\Http\Request([
            "id_product" => 2,
            "title" => $this->faker->paragraph(3),
            "is_title" => "0",
            "unit" => "Und",
            "quantity" => $this->faker->randomNumber(2, 1, 100),
            "unit_price" => $this->faker->randomNumber(4, 10, 5000),
            "subtotal" => $this->faker->randomNumber(5, 10, 50000),
            "costo_c_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_c_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "margen_b" => $this->faker->randomNumber(2, 0.1, 0.9),
            "pv_material" => $this->faker->randomNumber(2, 10, 500),
            "ut_x_unid" => $this->faker->randomNumber(2, 10, 500),
            "ut_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_kg_x_u" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_kg" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_dollar" => $this->faker->randomNumber(2, 10, 500)
        ]), $qp);

        /**Titulo 2 */
        $qp = new QuoteProduct();
        $qp->id_quote = 2;
        $qp->level = 2;
        $qp->save();

        $qpc = new QuoteProductController();
        $qpc->update(new \Illuminate\Http\Request([
            "id_product" => 1,
            "title" => $this->faker->paragraph(3),
            "is_title" => "1",
            "unit" => "Und",
            "quantity" => $this->faker->randomNumber(2, 1, 100),
            "unit_price" => $this->faker->randomNumber(4, 10, 5000),
            "subtotal" => $this->faker->randomNumber(5, 10, 50000),
            "costo_c_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_c_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "margen_b" => $this->faker->randomNumber(2, 0.1, 0.9),
            "pv_material" => $this->faker->randomNumber(2, 10, 500),
            "ut_x_unid" => $this->faker->randomNumber(2, 10, 500),
            "ut_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_kg_x_u" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_kg" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_dollar" => $this->faker->randomNumber(2, 10, 500)
        ]), $qp);

        /**Producto 2 */
        $qp = new QuoteProduct();
        $qp->id_quote = 2;
        $qp->level = 2.1;
        $qp->save();

        $qpc = new QuoteProductController();
        $qpc->update(new \Illuminate\Http\Request([
            "id_product" => 17,
            "title" => $this->faker->paragraph(3),
            "is_title" => "0",
            "unit" => "Und",
            "quantity" => $this->faker->randomNumber(2, 1, 100),
            "unit_price" => $this->faker->randomNumber(4, 10, 5000),
            "subtotal" => $this->faker->randomNumber(5, 10, 50000),
            "costo_c_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_c_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "margen_b" => $this->faker->randomNumber(2, 0.1, 0.9),
            "pv_material" => $this->faker->randomNumber(2, 10, 500),
            "ut_x_unid" => $this->faker->randomNumber(2, 10, 500),
            "ut_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_kg_x_u" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_kg" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_dollar" => $this->faker->randomNumber(2, 10, 500)
        ]), $qp);


        /**Instalación 2 */
        $qp = new QuoteProduct();
        $qp->id_quote = 2;
        $qp->level = 2.2;
        $qp->save();

        $qpc = new QuoteProductController();
        $qpc->update(new \Illuminate\Http\Request([
            "id_product" => 3,
            "title" => $this->faker->paragraph(3),
            "is_title" => "0",
            "unit" => "Und",
            "quantity" => $this->faker->randomNumber(2, 1, 100),
            "unit_price" => $this->faker->randomNumber(4, 10, 5000),
            "subtotal" => $this->faker->randomNumber(5, 10, 50000),
            "costo_c_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_c_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_dollar" => $this->faker->randomNumber(2, 10, 500),
            "costo_final_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "margen_b" => $this->faker->randomNumber(2, 0.1, 0.9),
            "pv_material" => $this->faker->randomNumber(2, 10, 500),
            "ut_x_unid" => $this->faker->randomNumber(2, 10, 500),
            "ut_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_kg_x_u" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_kg" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_u_dollar" => $this->faker->randomNumber(2, 10, 500),
            "flete_total_dollar" => $this->faker->randomNumber(2, 10, 500)
        ]), $qp);
    }
}
