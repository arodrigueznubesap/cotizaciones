<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_products', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('id_product')->nullable()->default(1);
            $table->unsignedBigInteger('id_quote');
            
            //$table->text('json_template_installation_data', 5000)->nullable(); 
            /*
                Parametros de la instalación -> [{}]
                
                datos -> [Area a trabajar -> float
                Cuadrillas -> integer
                M2 x Dias x Cuad -> float
                Holgura -> integer]
            */ 
            $table->float('level',7,2);
            $table->string('title')->nullable()->default("");
            $table->string('observation')->nullable()->default("");
            $table->smallInteger('is_title')->nullable()->default(0);  
            $table->string('unit')->nullable(); 
            $table->float('quantity',15,2)->nullable(); 
            $table->float('unit_price',15,2)->nullable(); 
            $table->float('subtotal',15,2)->nullable(); 
            $table->float('total_inst',15,2)->nullable();
            $table->float('total_inst_perso',15,2)->nullable(); 
            $table->float('total_inst_equip',15,2)->nullable(); 
            $table->float('total_inst_otros',15,2)->nullable();  
            $table->float('usd_x_m2_inst',15,2)->nullable(); 

            $table->float('subtotal_inst',15,2)->nullable(); 
            $table->float('ggenerales_inst',15,2)->default(10.00)->nullable();
            $table->float('Subtotal2_inst',15,2)->nullable();  
            $table->float('igv_inst',15,2)->nullable();
            

            $table->float('costo_c_u_dollar',15,2)->nullable();
            $table->float('costo_c_total_u_dollar',15,2)->nullable();
            $table->float('flete_dollar',15,2)->nullable();
            $table->float('costo_final_dollar',15,2)->nullable();
            $table->float('costo_final_u_dollar',15,2)->nullable();
            $table->float('margen_b',15,2)->nullable();
            $table->float('pv_material',15,2)->nullable();
            $table->float('ut_x_unid',15,2)->nullable();
            $table->float('ut_dollar',15,2)->nullable();
            $table->float('flete_kg_x_u',15,2)->nullable();
            $table->float('flete_total_kg',15,2)->nullable();
            $table->float('flete_total_u_dollar',15,2)->nullable();
            $table->float('flete_total_dollar',15,2)->nullable();
            
            



            $table->float('cuadrillas', 12,2)->nullable();
            $table->float('m2_day_cuadrillas', 12,2)->nullable();
            $table->integer('holgura')->nullable();
            $table->float('area_trabajo', 12,2)->nullable();
            $table->integer('total_days')->nullable();
            $table->timestamps();


            $table->foreign('id_product')->references('id')->on('products');
            $table->foreign('id_quote')->references('id')->on('quotes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_products');
    }
};
