<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->string('description');
            $table->string('unit')->nullable();
            $table->unsignedBigInteger('id_product_brand')->nullable();
            $table->unsignedBigInteger('id_product_type')->nullable();
            $table->unsignedBigInteger('id_installation_type')->nullable();
            $table->float('unit_price',12,2)->default(0);
            $table->timestamps();


            $table->foreign('id_product_brand')->references('id')->on('product_brands');
            $table->foreign('id_product_type')->references('id')->on('product_types');
            $table->foreign('id_installation_type')->references('id')->on('installation_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
