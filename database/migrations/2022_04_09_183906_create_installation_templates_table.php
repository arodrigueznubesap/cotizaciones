<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('installation_templates', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_installation_type');
            $table->unsignedBigInteger('id_installation_parameter');
/*             $table->string('unit');
            $table->float('cant',10,2);
            $table->integer('days'); */
            $table->timestamps();


            $table->foreign('id_installation_type')->references('id')->on('installation_types');
            $table->foreign('id_installation_parameter')->references('id')->on('installation_parameters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('installation_templates');
    }
};
