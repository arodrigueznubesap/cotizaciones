<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_conditions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_condition');
            $table->unsignedBigInteger('id_quote');
            $table->float('level');
            $table->integer('is_title');
            $table->text('content');
            $table->string('title');
            $table->boolean('state');
            $table->timestamps();


            $table->foreign('id_condition')->references('id')->on('conditions');
            $table->foreign('id_quote')->references('id')->on('quotes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_conditions');
    }
};
