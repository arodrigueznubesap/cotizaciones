<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quality_controls', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_quote');
            $table->unsignedBigInteger('id_quality_attribute');
            $table->boolean('state');
            $table->timestamps();


            $table->foreign('id_quote')->references('id')->on('quotes');
            $table->foreign('id_quality_attribute')->references('id')->on('quality_attributes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quality_controls');
    }
};
