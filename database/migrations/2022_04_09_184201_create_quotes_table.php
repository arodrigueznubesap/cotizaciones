<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{  
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotes', function (Blueprint $table) {
            $table->id();
            $table->integer('id_currency'); //1->soles, 2->dolares
            $table->double('exchange_rate');
            $table->string('code');
            $table->text('reference');
            $table->date('quote_date');
            $table->boolean('has_igv');
            $table->float('igv_rate',5,2)->nullable();
            $table->float('igv',10,2)->nullable();
            $table->float('aduana_rate',5,2)->nullable();
            $table->float('aduana',10,2)->nullable();
            $table->float('insurance_rate',5,2)->nullable();
            $table->float('insurance',10,2)->nullable();
            $table->float('total',15,2)->nullable()->default(0);
            $table->float('subtotal_general',15,2)->nullable()->default(0);
            //$table->float('gross_margin');
            $table->unsignedBigInteger('id_user');
            $table->unsignedBigInteger('id_customer');
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_customer')->references('id')->on('customers');
        });
    }

    public function down(){
        Schema::dropIfExists('quotes');
    }
};