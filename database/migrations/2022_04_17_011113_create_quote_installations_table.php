<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_installations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_quote_products');
            $table->unsignedBigInteger('id_installation_parameter');
            $table->string('report_description');
            $table->float('quantity',12,3)->default(0)->nullable();
            $table->integer('days')->default(0)->nullable();
            $table->integer('level_class')->default(0)->nullable();
            $table->float('dol_x_unit',12,2)->default(0)->nullable();
            $table->float('sol_x_unit',12,2)->default(0)->nullable();
            $table->float('dollar_total',12,2)->default(0)->nullable();
            $table->float('sol_total',12,2)->default(0)->nullable();
            $table->timestamps();

            $table->foreign('id_quote_products')->references('id')->on('quote_products');
            $table->foreign('id_installation_parameter')->references('id')->on('installation_parameters');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_installation');
    }
};
